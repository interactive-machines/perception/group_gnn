## Match n' Mingle

#### Where to download the data

Download the data from the `Match n' Mingle` shared drive. You must obtain permission from the original authors of the Match n' Mingle dataset.

Put this data in the `data` folder in a folder named `matchnmingle`.

### Folder structure

When downloading the data, make sure it has this structure - this is the default folder structure when downloading the data:

```
matchnmingle
    manual_annotations
        CAMERA.csv
        DATA.csv
	    LABELS.csv
        LOST.csv
        PARTICIPANTS.csv
        F-formationsGT
            Day1.csv
            Day2.csv
            Day3.csv
    wearable_acceleration
	    day1_subject{i}.csv
```

### create_cutouts.py

If you want image features included in the model, you must run this script first to create the image cutouts from the videos and bounding boxes.
Takes the following arguments:

- `-c`, `--camera-data-file`: Path to camera data file- in matchnmingle data, should be CAMERA.csv.
- `-bb`, `--bbox-data-file`: Path to bounding box data file- in matchnmingle data, should be DATA.csv.
- `-v`, `--downsample-video-folder`: Path to folder with downsampled videos.
- `-s`, `--cutout-save-folder`: Path to folder to save image cutouts. If not provided, will not create or save image cutouts

Example command:

```
python3 data_preprocessing/matchnmingle/create_cutouts.py --camera-data-file <path/to/matchnmingle>/manual_annotations/CAMERA.csv --bbox-data-file <path/to/matchnmingle>/manual_annotations/DATA.csv --downsample-video-folder <path/to/matchnmingle>/videos/downsample --cutout-save-folder <path/to/matchnmingle>/image_cutouts
```

This will save a `.npy` file with the cutout images that are 30x30 pixels to `<path/to/matchnmingle>/image_cutouts/30x30_cutouts.npy`.

### convert_mnm.py

Run script to generate the condensed `features.txt` and `groups.txt` from the raw data.
Takes the following arguments:

- `--mnm-folder`: Folder with mnm data. Assumes a folder organization identical to original download.
- `--cutout-file`: Path to npy file with saved cutouts. If not provided, will not include image features.
- `--resnet-layers`: Number of resnet layers to use when embedding cutout images. One of [4, 8]. This will correspond to [4096, 512] image features, respectively. Default 8.
- `--include-labels`: Whether to include one-hot encoding of action labels in features. Labels will not be included if flag not given.
- `--include-accel`: Whether to include acceleration data in features. Acceleration will not be included if flag not given.
- `--include-bbox`: Whether to include bounding box data (width and height) in features. Bounding box data will not be included if flag not given.
- `--save-folder`: Path to folder to save data.

Example command:

```
python3 data_preprocessing/matchnmingle/convert_mnm.py --mnm-folder <path/to/matchnmingle> --save-folder <path/to/matchnmingle>/processed --cutout-file <path/to/matchnmingle>/image_cutouts/30x30_cutouts.npy --resnet-layers 8 --include-labels --include-accel --include-bbox
```

python3 data_preprocessing/matchnmingle/convert_mnm.py --mnm-folder /Users/sydney/research/group_detection/eec-gnn/data/matchnmingle/raw --save-folder /Users/sydney/research/group_detection/group_gnn/data/matchnmingle/processed --include-labels --include-accel --include-bbox

This will create `<path/to/matchnmingle>/processed/mnm_features_groups_labels_accel_bbox_imgs_8_layers/features.txt` and `<path/to/matchnmingle>/processed/mnm_features_groups_labels_accel_bbox_imgs_8_layers/groups.txt`, which contain the features for each person and group annotations, respectively, in Cocktail Party dataset format.

## Generated data

The generated data is in the same format as the Cocktail Party dataset.

### features.txt

Contains rows of the form
`{day}-{cam}-{global_frame_id}-{frame_id} ID_{person_id} {x} {y} ({action}) ({has_accelerometer} {accelerometer_1} ... {accelerometer_10}) ({height} {width}) ({img_features}) ID_{person_id} ...`
where each row corresponds to a unique frame and containes a space-separated list of all the features for each person in the frame.

- day - in {0,1,2}, corresponds to 0-based index of the day the recording was made
- cam - in {0,1,2}, corresponds to 0-based index of the camera which the frame is in
- global_frame_id - the unique id for the frame. These are not repeated globally
- frame_id - the frame number from the camera - not necessarily unique
- person_id - the global id for the participant
- x - x position in the camera frame - computed from the center of the annotated bounding box
- y - y position in the camera frame - computed from the center of the annotated bounding box
- action - 9-dimensional 1-hot encoded vector of the annotated action of the participant. These
  actions and their corresponding embedding indices are:
  0-Walking
  1-Stepping
  2-Drinking
  3-Speaking
  4-Hand_Gesture
  5-Head_Gesture
  6-Laugh
  7-Hair_Touching
  8-Action_Occluded
  The action encoding is only included if --include-labels was passed to `convert_mnm.py`
- has_accelerometer - a boolean value indicating whether the participant had a working accelerometer. If 0,
  all accelerometer data will be 0.
  The accelerometer boolean is only included if --include-accel was passed to `convert_mnm.py`
- acceleometer_n - the n-th preceding triaxial accelerometer data. That is, `accelerometer_3` containes
  `x`, `y`, and `z` accelerations for the annotation acceleration that occured 3 annotations before
  the frame. Accelerometer annotations occur at 20 Hz, so `{accelerometer_n}` has the annotations
  for `n/20` seconda before the current frame.
  The accelerometer data is only included if --include-accel was passed to `convert_mnm.py`
- height - height of the annotated bounding box. The bbox height is only included if --include-bbox was passed to `convert_mnm.py`
- width - width of the annotated bounding box. The bbox width is only included if --include-bbox was passed to `convert_mnm.py`
- img_features - 512 or 4096-dimensional embedding of the top-down view of the person. These images
  are cut from the annotated bounding box and the features are computed from the
  torchvision resnet18 model (all but the classification layer). There are 512 image features if using 8 ResNet layers and 4096 if using 4.

## groups.txt

Contains rows of the form  
`{day}-{cam}-{global_id}-{frame_id} <ID_{participant_id} ... ID_{partipant_id}> ... <...>`

Groups are space-separated and denoted by brackets, `<>`. Each participant id in the group
