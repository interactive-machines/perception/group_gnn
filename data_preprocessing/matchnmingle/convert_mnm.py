import argparse
import sys
import os

from tqdm import tqdm
import numpy as np
import cv2

import torch
from torch import nn
from torchvision import models

# For windows
# sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from read_mnm import read_dataset

def get_args():
    parser = argparse.ArgumentParser(description='Create cutouts of people from camera and bounding box information.')

    parser.add_argument('--mnm-folder', type=str, required=True,
                        help='Folder with mnm data. Assumes a folder organization identical to original download.')
    parser.add_argument('--cutout-file', type=str, default=None,
                        help='Path to npy file with saved cutouts. If not provided, will not include image features.')
    parser.add_argument('--resnet-layers', type=int, choices=[4,8], default=8,
                        help='Number of resnet layers to use when embedding cutout images. One of [4, 8]. This will correspond to [4096, 512] image features, respectively. Default 8.')
    parser.add_argument('--include-labels', action='store_true',
                        help='Whether to include one-hot encoding of action labels in features. Labels will not be included if flag not given.')
    parser.add_argument('--include-accel', action='store_true',
                        help='Whether to include acceleration data in features. Acceleration will not be included if flag not given.')
    parser.add_argument('--include-bbox', action='store_true',
                        help='Whether to include bounding box data (width and height) in features. Bounding box data will not be included if flag not given.')
    parser.add_argument('--save-folder', type=str, default=None,
                        help='Path to folder to save data.')

    args = parser.parse_args()
    return args

def process_cutout(image_embedding_model, image):
    img_features =  torch.flatten(image_embedding_model(
        torch.tensor(np.expand_dims(cv2.resize(image, (30,30)).transpose(2, 0, 1), axis=0),
                                  dtype=torch.float) / 255
    ))
    # print('image_features.shape[0]:', img_features.shape[0])
    assert (img_features.shape[0] == 512 or img_features.shape[0] == 4096)
    return img_features

def process_cutouts(image_embedding_model, images):
    processed_cutouts = {}
    for ID in images:
        image_features = process_cutout(image_embedding_model, images[ID])
        # if image_features.shape[0] != 512:
        #     print(f'ID: {ID} - image_features.shape: {image_features.shape} - images[ID].shape: {images[ID].shape}')
        valid_num_image_features = [512, 4096]
        if image_features.shape[0] not in valid_num_image_features:
            print(f'Invalid number of image features - should be one of {valid_num_image_features} - found number of features: {image_features.shape[0]}')
            return None
        processed_cutouts[ID] = image_features

    return processed_cutouts

def filter_sub_formations(all_groups):
    filtered_groups = {}
    for frame in all_groups.keys():
        any_sub_group = False
        for i, group in enumerate(all_groups[frame]):
            if (i+1) >= len(all_groups[frame]):
                continue
            sub_group = np.sum([len(set(other_group).intersection(group)) != 0 for other_group in all_groups[frame][i+1:]]) > 0
            any_sub_group = (any_sub_group or sub_group)
        if not any_sub_group:
            filtered_groups[frame] = all_groups[frame]
    return filtered_groups

def make_feature_line(frame, features, image_features):
    data_line = f'{frame}'
    for id in features:
        features_str = f' ID_{id}'
        for feature in features[id]:
            features_str += f' {feature}'
        if image_features is not None:
            for i in range(image_features[id].shape[0]):
                    features_str += f' {image_features[id][i]}'
        data_line += features_str

    num_people = len(features.keys())
    # num_raw_features = 1 + 4 + 1 + 3*10

    # correct_data_len = [(num_people * (512 + num_raw_features)) + 1), (num_people * (4101 + num_raw_features)) + 1)] if image_features is not None else 
    # assert ((len(data_line.split(' ')) == (num_people * (512 + num_raw_features)) + 1) or (len(data_line.split(' ')) == (num_people * (4101 + num_raw_features)) + 1))
    return data_line

def make_group_line(frame, groups, features):
    # Example: 1275403283.267134 < ID_006 ID_001 > < ID_002 ID_003 ID_004 ID_005 >
    data_line = f'{frame}'
    found_people = set()
    for group in groups:
        data_line += f' <'
        for person in group:
            found_people.add(person)
            data_line += f' ID_{person}'
        data_line += ' >'

    all_people = set(features.keys())
    leftover_people = all_people - found_people
    for person in leftover_people:
        data_line += f' < ID_{person} >'

    return data_line

if __name__ == '__main__':
    args = get_args()
    output_folder = 'mnm_features_groups'
    if args.include_labels:
        output_folder += '_labels'
    if args.include_accel:
        output_folder += '_accel'
    if args.include_bbox:
        output_folder += '_bbox'
    if args.cutout_file is not None:
        output_folder += f'_imgs_{args.resnet_layers}_layers'
    output_path = os.path.join(args.save_folder, output_folder)
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    all_groups, all_features, all_people, all_cutouts = read_dataset(args.mnm_folder, cutout_file=args.cutout_file, include_labels=args.include_labels,
                                                                     include_accel=args.include_accel, include_bbox=args.include_bbox)
    print('all_groups:', len(all_groups))
    print('all_features:', len(all_features))
    print('all_people:', len(all_people))
    if all_cutouts is not None:
        print('all_cutouts:', len(all_cutouts))
        print('all_cutouts.keys()[0]:', list(all_cutouts.keys())[0])

        unique_keys = set(all_cutouts.keys()) - set(all_groups.keys())
        print('unique_keys:', len(unique_keys))

    print('unique feature views:', set([key[:3] for key in all_features.keys()]))
    print('unique group views:', set([key[:3] for key in all_groups.keys()]))

    if args.cutout_file is not None:
        # args.resnet_layers is the number of layers of the resnet18 to use
        # One of [4, 8]. This will correspond to [4096, 512] image features
        resnet18 = models.resnet18(pretrained=True)
        image_embedding_model = nn.Sequential(*(list(resnet18.children())[0:args.resnet_layers]))
        image_embedding_model.eval()

    num_examples = 0
    feature_file_path = os.path.join(output_path, 'features.txt')
    feature_file = open(feature_file_path, 'a')
    feature_file.truncate(0)
    group_file_path = os.path.join(output_path, 'groups.txt')
    group_file = open(group_file_path, 'a')
    group_file.truncate(0)

    filtered_groups = filter_sub_formations(all_groups)
    print('original group length:', len(all_groups))
    print('filtered group length:', len(filtered_groups))
    print('removed groups:', len(all_groups) - len(filtered_groups))

    unique_views = set()
    try:
        examples = []
        keys = list(filtered_groups.keys())
        for i in tqdm(range(len(filtered_groups))):
            frame = keys[i]
            num_examples += 1

            unique_views.add(frame[:3])
            group = filtered_groups[frame]
            features = all_features[frame]
            people = all_people[frame]

            if args.cutout_file is not None:
                image_features = process_cutouts(image_embedding_model, all_cutouts[frame])
                if image_features is None: # image_features should not be none if args.cutout_file is provided
                    continue
            else:
                image_features = None

            # add line to feature file
            feature_line = make_feature_line(frame, features, image_features)
            feature_line += '\n'
            feature_file.write(feature_line)

            # add line to group file
            group_line = make_group_line(frame, group, features)
            group_line += '\n'
            group_file.write(group_line)
    finally:
        print('unique_views:', unique_views)
        print('num_examples:', num_examples)

        feature_file.close()
        group_file.close()
