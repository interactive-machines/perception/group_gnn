import os

import tqdm
import csv
import numpy as np

from utils import DAY_START_TIMES, PERSON_IDS_BY_DAY

def read_participants(participant_file):
    participants = {1: {}, 2: {}, 3: {}}
    with open(participant_file, 'r') as f:
        reader = csv.reader(f)
        for global_id, row in enumerate(reader):
            if len(row) != 0:
                participants[int(row[0])][int(row[1])] = global_id
    return participants

def read_camera(camera_file):
    with open(camera_file, 'r') as f:
        camera_data = [[int(x) for x in row] for row in csv.reader(f) if len(row) != 0]
    return camera_data


def read_features(feature_file):
    with open(feature_file, 'r') as f:
        feature_data = [[int(x) for x in row] for row in csv.reader(f) if len(row) != 0]
    return feature_data

def read_cutouts(cutouts_file):
    cutouts = np.load(cutouts_file, allow_pickle=True).item()
    return cutouts

def process_group_file(group_file, annotatation_start):
    annotation_end = annotatation_start + 10*60
    groups = {i: [] for i in range(annotatation_start, annotation_end)}
    with open(group_file, 'r') as f:
        reader = csv.reader(f)
        # skip header line
        next(reader)
        for row in reader:
            group = [int(x) for x in row[0].split()]
            start, end = int(row[3]), int(row[4])
            for i in range(start, end):
                groups[i].append(group)
    return groups

def read_accelerations(accel_dir, participants):
    accelerations = {}
    for root_dir, sub_dirs, files in os.walk(accel_dir):
        for accel_filename in files:
            if 'subject' not in accel_filename:
                continue
            day = int(accel_filename[3])
            subj_id = int(accel_filename[12:-4])
            global_id = participants[day][subj_id]

            accel_path = os.path.join(root_dir, accel_filename)
            accel_data = read_accel_file(accel_path)
            accelerations[global_id] = accel_data
    return accelerations

def read_accel_file(accel_path):
    try:
        with open(accel_path, 'r') as f:
            accel_data = [[float(x) for x in row] for row in csv.reader(f) if len(row) != 0]
    except FileNotFoundError as e:
        print(e)
        accel_data = None
    return accel_data

def get_label_features(frame, global_id, labels, window=10):
    if frame == 0:
        return [0,0,0,0,0,0,0,0,0]
    frame_labels = labels[frame-window:frame]
    subj_labels = [frame_labels[9*global_id: 9*global_id+9] for frame_labels in frame_labels]
    agg_labels = np.array(subj_labels).max(axis=0)
    return list(agg_labels)

def get_accel_features(frame, person_idx, accelerations, window=10):
    # bool for whether acceleration data is valid
    if frame == 0:
        accel_data = None
    else:
        accel_data = accelerations[person_idx] if person_idx in accelerations else None

    # acceleration data
    if accel_data:
        found_accel_file = [1]
        frame_accel_data = [x for sub in accel_data[frame-window:frame] for x in sub[1:]]
    else:
        found_accel_file = [1]
        frame_accel_data = [0 for _ in range(3*window)]

    accel_features = found_accel_file + frame_accel_data
    return accel_features

def read_group_file(group_file):
    groups = []
    with open(group_file, 'r') as f:
        reader = csv.reader(f)
        # skip header line
        next(reader)
        for row in reader:
            # ignore empty lines
            if len(row) == 0:
                continue

            group = [int(x) for x in row[0].split()]
            start, end = int(row[3]), int(row[4])
            groups.append((group, start, end))
    return groups

def split_group_by_camera(frame, day, group, participants, camera_values, bbox_values):
    '''
    Split group by camera - the fformations are annotates without respect to which people are visible in which
    camera. Because we are not able to reason across cameras, we split a group that is spread across multiple
    cameras into multiple groups and consider the people visible in a single camera

    Args:
        frame: frame number for group annotation
        day: one-indexed day corresponding to which day the data was recorded on - one of [1,2,3]
        group: list of participant ids belonging to one group
        participants: dictionary mapping frame to global participant id
        bbox_values: dictionary mapping frame to participant bounding box
    '''
    groups_by_camera = {1: [], 2: [], 3: []}

    # Avoiding participants with a 0 camera value and bbox features all equal to 0
    for x in group:
        participant_id = participants[day][x]
        camera_number = camera_values[frame][participant_id]

        participant_bbox_feature_start_idx = 7 * participant_id + 1
        participant_bbox_feature_end_idx = 7 * participant_id + 5
        participant_bbox_features = bbox_values[frame][participant_bbox_feature_start_idx:participant_bbox_feature_end_idx]
        if camera_number != 0 and participant_bbox_features!= [0, 0, 0, 0]:
            groups_by_camera[camera_number].append(x)
    return groups_by_camera

def process_person_features_for_frame(frame, day, camera, camera_values, bbox_values, include_bbox, labels, accelerations):
    features_processed = {}
    # Finding everyone else in the frame, with features that aren't all 0
    for person_id, person_camera in enumerate(camera_values[frame]):
        bbox = bbox_values[frame][7*person_id+1: 7*person_id+5]

        # skip if camera doesnt match or bounding box is all 0s
        if person_camera != camera or bbox == [0, 0, 0, 0]:
            continue

        # Checks if the person is in the correct day
        day_id_range = PERSON_IDS_BY_DAY[f'day_{day}']
        if person_id in day_id_range:
            pos_features = [(bbox[0] + bbox[2]) / 2,  # x
                            (bbox[1] + bbox[3]) / 2]  # y
            subj_features = pos_features
            if labels is not None:
                label_features = get_label_features(frame, person_id, labels)
                subj_features += label_features
            if accelerations is not None:
                accelerometer_features = get_accel_features(frame, person_id, accelerations)
                subj_features += accelerometer_features
            if include_bbox:
                bounding_box_features = [bbox[2] - bbox[0],  # width
                                        bbox[3] - bbox[1]]  # height
                subj_features += bounding_box_features

            features_processed[person_id] = subj_features
    return features_processed

def process_features_and_groups(group_files, participants, camera_values, bbox_values, include_bbox, labels, accelerations):
    """
    processes all F-formationsGT files and features into features and groups

    Args:
    :param group_files: list of group file names from the 3 days
    :param participants: dict of 3 dicts (one per day), mapping dailyID to globalID
    :param camera_values: matrix of camera values [frame x num_participants]
    :param bbox_values: matrix of all feature values [frame x num_participants*7]
    :param incllude_bbox: bool whether to include width and height of bounding box in addition to x,y position in features
    :param labels: list of lists containing action labels [frame x num_participants*9]. If None, will not include labels in person features
    :param accelerations: list of lists containing acceleration data [frame x num_participants*3]. If None, will not include acceleration in person features

    Returns
    'all_groups': a dict mapping frame to annotated groupings
    'all_features': a dict mapping frame to features of all visible participants. Features are [<position> <labels>(if labels is not None) <acceleration>(if acceleration is not None) <bbox>(if include_bbox)]
    'all_people': a dict mapping frame to number of visible participants
    """
    all_groups = {}
    all_features = {}
    all_people = {}

    sub_f_formations = 0
    for day in range(1, len(group_files) + 1):
        print('day:', day)
        annotation_start_time = DAY_START_TIMES[f'day_{day}']

        annotated_groups = read_group_file(group_files[day - 1])
        for group, start, end in tqdm.tqdm(annotated_groups):
            for frame in range(start, end + 1, 20):
                groups_by_camera = split_group_by_camera(frame, day, group, participants, camera_values, bbox_values)

                for camera in groups_by_camera:
                    # Include a camera in a frame if it contains at least 2 people
                    if len(groups_by_camera[camera]) <= 1:
                        continue

                    frame_id = int(1803 * (camera - 1) + 601 * (day - 1) + (frame - annotation_start_time) / 20)
                    new_frame = f'{day-1}-{camera-1}-{frame_id}-{frame}'
                    
                    groups_processed = [participants[day][x] for x in groups_by_camera[camera]]
                    if new_frame in all_groups: # check if group is sub-fformation or new annotation
                        processed_set = set(groups_processed)
                        new_group = np.sum([set(group) == processed_set for group in all_groups[new_frame]]) == 0
                        sub_group = np.sum([len(set(group).intersection(processed_set)) != 0 for group in all_groups[new_frame]]) > 0
                        if sub_group:
                            sub_f_formations += 1
                        if new_group:
                            all_groups[new_frame].append(groups_processed)
                    else:
                        all_groups[new_frame] = [groups_processed]

                    if new_frame not in all_features:
                        person_features = process_person_features_for_frame(frame, day, camera, camera_values, bbox_values, include_bbox, labels, accelerations)
                        all_features[new_frame] = person_features
                        all_people[new_frame] = len(all_features[new_frame])

    print('sub-f-formations:', sub_f_formations)
    print('unique day/camera combos processed:', set([key[:3] for key in all_features.keys()]))
    return all_groups, all_features, all_people

def read_dataset(data_dir, cutout_file, include_bbox, include_labels, include_accel):
    '''
    Args:
        data_dir: directory where match n' mingle dataset is
        cutout_file: Path to npy file with cutout top-down images of people. If None, will not use cutouts and all_cutouts will be None.
        include_bbox: bool indicating whether to include bounding box features (width and height) in person fetures. Position is always included
        include_labels: bool indicating whether to include action label features in person features
        include_accel: bool indicating whether to include acceleration features in person features

    Returns:
        all_groups: dict of list of lists - keys are frame ids, list has sublists which are ids of people in groups
        all_features: dict of dict of lists - keys are frame ids, second key is id of person, list is features [x, y, width, height]
        all_people: dict of ints - keys are frame ids, values are number of people
        all_cutouts: dict of dict to [height, width, 3] numpy array - keys are frame ids, second key is id of person.
            If cutout_file is None, this is None
    '''
    accelerometer_dir = os.path.join(data_dir, "wearable_acceleration")
    participant_file = os.path.join(data_dir, "manual_annotations/PARTICIPANTS.csv")
    camera_file = os.path.join(data_dir, "manual_annotations/CAMERA.csv")
    bbox_file = os.path.join(data_dir, "manual_annotations/DATA.csv")
    labels_file = os.path.join(data_dir, "manual_annotations/LABELS.csv")
    group_files = [os.path.join(data_dir, "manual_annotations/F-formationsGT/Day1.csv"),
                   os.path.join(data_dir, "manual_annotations/F-formationsGT/Day2.csv"),
                   os.path.join(data_dir, "manual_annotations/F-formationsGT/Day3.csv")]

    # temporary dict of 3 dicts (one per day), mapping dailyID to globalID
    print('Reading particpants file')
    participants = read_participants(participant_file)

    # temporary matrix of camera values
    print('Reading camera file')
    camera_values = read_camera(camera_file)

    # temporary matrix of all position values
    print('Reading bounding box features file')
    bbox_values = read_features(bbox_file)

    if include_labels:
        # matrix of all action labels
        print('Reading action labels file')
        labels = read_features(labels_file)
    else:
        labels = None

    if include_accel:
        # get accelerations for all participants with valid data
        print('Reading acceleration features file')
        accelerations = read_accelerations(accelerometer_dir, participants)
    else:
        accelerations = None

    # Processing position features by frame for each annotated grouping
    all_groups, all_features, all_people = process_features_and_groups(group_files, participants, camera_values, bbox_values, include_bbox, labels, accelerations)

    if cutout_file is not None:
        # Loading cutouts
        all_cutouts = read_cutouts(cutouts_file)
    else:
        all_cutouts = None

    if len(all_groups) != len(all_features):
        raise ValueError("Mismatched lengths")
    return all_groups, all_features, all_people, all_cutouts
