
# this comes from the MatchN'Mingle readme. In each video:
# -Day 1: from 12min to 22min (of the 30min video)
# -Day 2: from 0min to 10min (of the 30min video)
# -Day 3: from 18min to 28min (of the 30min video)
DAY_START_TIMES = {
    'day_1': 12000,
    'day_2': 0,
    'day_3': 21600
}
# person ids are unique and correspond to the index of the bounding boxes in bbox_data_file
PERSON_IDS_BY_DAY = {
    'day_1': range(0, 32), 
    'day_2': range(32, 62), 
    'day_3': range(62, 92)
}