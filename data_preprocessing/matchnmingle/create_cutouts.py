import argparse
import os
import csv
from tqdm import tqdm

import numpy as np
import cv2

from .utils import DAY_START_TIMES, PERSON_IDS_BY_DAY

def get_args():
    parser = argparse.ArgumentParser(description='Create cutouts of people from camera and bounding box information.')

    parser.add_argument('-c', '--camera-data-file', type=str, required=True,
                        help='Path to camera data file- in matchnmingle data, should be CAMERA.csv.')
    parser.add_argument('-bb', '--bbox-data-file', type=str, required=True,
                        help='Path to bounding box data file- in matchnmingle data, should be DATA.csv.')
    parser.add_argument('-v', '--downsample-video-folder', type=str, required=True, 
                        help='Path to folder with downsampled videos.')
    parser.add_argument('-s', '--cutout-save-folder', type=str, required=True,
                        help='Path to folder to save image cutouts. If not provided, will not create or save image cutouts.')

    args = parser.parse_args()
    return args

def load_cutout_data(camera_data_file, bbox_data_file):
    with open(camera_data_file, 'r') as f:
        camera_data = np.array([[int(x) for x in line] for line in csv.reader(f)])

    with open(bbox_data_file, 'r') as f:
        feature_data = np.array([[int(x) for x in line] for line in csv.reader(f)])
    return camera_data, feature_data

def create_cutouts(video_file_name, camera_data_file, bbox_data_file, day_index, cam_index, cutout_size=(30,30)):
    start_time = DAY_START_TIMES[f'day_{day+1}']
    end_time = start_time + 12001
    person_id_range = PERSON_IDS_BY_DAY[f'day_{day_index+1}']

    video = cv2.VideoCapture(video_file_name)
    video.set(cv2.CAP_PROP_POS_FRAMES, start_time)

    camera_data, bbox_data = load_cutout_data(camera_data_file, bbox_data_file)

    cutout_dict = {}
    for frame in tqdm(range(start_time, end_time, 20)):
        ret, full_image = video.read()

        frame_id = int(1803 * cam_index + 601 * day_index + (frame - start_time) / 20)  # frame ordering
        new_frame = f'{day_index}-{cam_index}-{frame_id}-{frame}'
        cutout_dict[new_frame] = {}

        # Append cutout of people in the correct camera with non-0 feature values
        for i in id_range:
            bbox = bbox_data[frame, i * 7 + 1: i * 7 + 5]

            if bbox is None:
                print(f'i: {i} - bbox: {bbox}')

            if camera_data[frame][i] == cam_index + 1 and not np.all(bbox == 0):
                y_start, y_stop = bbox[1], bbox[3]
                x_start, x_stop = bbox[0], bbox[2]
                cutout = full_image[y_start:y_stop, x_start:x_stop]

                normalized_cutout = cv2.resize(cv2.cvtColor(cutout, cv2.COLOR_BGR2RGB), cutout_size)
                cutout_dict[new_frame][i] = np.array(normalized_cutout)

    return cutout_dict

if __name__ == '__main__':
    args = get_args()

    all_cutouts = {}    
    for cam in range(3):
        for day in range(3):
            print(f'Processing day {day+1} - camera {cam + 1}')
            video_path = os.path.join(args.downsample_video_folder, f"30min_day{day+1}_cam{cam + 1}_20fps_960x540.MP4")
            cutout_images = create_cutouts(video_path, args.camera_data_file, args.bbox_data_file, day, cam, cutout_size)

            if len(all_cutouts) > 0:
                all_cutouts = {**all_cutouts, **cutout_images}
            else:
                all_cutouts = {**cutout_images}

    cutout_save_path = os.path.join(args.cutout_save_folder, f"{size[0]}x{size[1]}_cutouts.npy")
    print(f'Created {len(all_cutouts)} image cutouts - saving to {cutout_save_path}')
    if not os.path.exists(cutout_save_path):
        os.makedirs(cutout_save_path)
    np.save(cutout_save_path, all_cutouts)