# Group Detection with GNNs

If you use this code, please [cite our paper](https://interactive-machines.com/assets/papers/thompson-ICMI21.pdf).

## Datasets
The cocktail party dataset is publicly accessible [here](https://tev.fbk.eu/resources/cocktailparty).

The MatchN'Mingle datasets are also publicly available, but you'll need to sign an EULA and ask the original authors' permission to use it. You can find that dataset [here](https://matchmakers.ewi.tudelft.nl/matchnmingle/pmwiki/pmwiki.php?n=Main.TheDataset). The instructions for preprocessing the MatchN'Mingle dataset are found in `data_preprocessing/matchnmingle/README_matchnmingle.md`.

## Setup

### Python

Install [Python 3.8](https://www.python.org/downloads/release/python-380/).

#### Ubuntu 18.04

If you are using Ubuntu 18.04, be sure not to upgrade your base python version, but install a new version of python. Try the following:

`sudo apt update`
`sudo apt install python3.8 python3.8-venv python3-venv`
`sudo apt install python3.8-distutils`

### Dependency Install

Run `./install.sh` to create a Python3.8 virtual environment called `.venv3.8` and install [pytorch geometric](https://pytorch-geometric.readthedocs.io/en/latest/notes/installation.html) and other dependencies. Currently using torch 1.8.1.

This script will detect the CUDA version installed on the machine and install the corresponding version of pytorch geometric. See [pytorch-geometric docs](https://pytorch-geometric.readthedocs.io/en/latest/notes/installation.html) for more details on compatable CUDA versions.

### Update .bashrc

Add or verify that these lines are in your .bashrc files, replacing `<cuda-version>` with your installed version of CUDA.

```
export PATH=/usr/local/cuda-<cuda-version>/bin:${PATH}
export CUDA_HOME=/usr/local/cuda-<cuda-version>
export LD_LIBRARY_PATH=/usr/local/cuda-<cuda-version>/lib64:$LD_LIBRARY_PATH
```

Example for CUDA 10.2

```
export PATH=/usr/local/cuda-10.2/bin:${PATH}
export CUDA_HOME=/usr/local/cuda-10.2
export LD_LIBRARY_PATH=/usr/local/cuda-10.2/lib64:$LD_LIBRARY_PATH
```

## Usage

Start virtual environment

```buildoutcfg
source .venv3.8/bin/activate
```

Start a screen to prevent accidentally killing the model

```buildoutcfg
screen
```

Start tensorboard

```
tensorboard --logdir=./logs  # keep running
```

Run model
on cpu:

```buildoutcfg
python main.py --no-cuda --dataset=matchnmingle --model=dante --weight-decay=0.01 --dropout=0.1
```

on gpu:

```buildoutcfg
CUDA_VISIBLE_DEVICES=0 python main.py --dataset=matchnmingle --model=dante --weight-decay=0.01 --dropout=0.1
```
