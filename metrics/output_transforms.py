import torch

from transforms import y_to_A_list, A_to_A_list


def output_transform_gnn(output, threshold):
    y_pred, y = output

    y_new, y_pred_new = [], []
    for y_pred_item, y_item in zip(y_pred, y):
        y_new.append(y_to_A_list(y_item))
        y_pred_new.append((A_to_A_list(y_pred_item) > threshold).float())
    return torch.cat(y_pred_new), torch.cat(y_new)


def output_transform_dante(output, threshold):
    y_pred, y = output
    y_pred_new, y_new = [], []
    for y_pred_item, y_item in zip(y_pred, y):
        mask = y_item >= 0
        y_item = y_item[mask]
        y_pred_item = y_pred_item[mask]
        y_pred_new.append((y_pred_item > threshold).float())
        y_new.append(y_item)
    y_pred = torch.cat(y_pred_new)
    y = torch.cat(y_new)
    return y_pred, y
