import os
import json
import random
from datetime import datetime

import torch
import torch.cuda
import torch.nn as nn
from torch.optim import Adam
from torch.utils.data import DataLoader, WeightedRandomSampler

from torch_geometric.data import DataLoader as GeometricDataLoader
from torch_geometric.transforms import Compose, Center, Distance, RadiusGraph

from ignite.engine import Events, create_supervised_trainer, create_supervised_evaluator
from ignite.handlers import ModelCheckpoint, EarlyStopping, global_step_from_engine
from ignite.metrics import Loss, Accuracy, Recall

from ignite.contrib.handlers import ProgressBar
from ignite.contrib.handlers.tensorboard_logger import TensorboardLogger, OutputHandler
from ignite.contrib.handlers.param_scheduler import LRScheduler
from torch.optim.lr_scheduler import StepLR

from helper.get_arguments import get_arguments
from transforms import y_to_A_list, A_to_A_list

from datasets.gnn.utils import (
    get_indices,
    split_train_test_by_camera_day,
    sandwich_splitting,
)
from datasets.gnn.cocktail_party.dataset import (
    CocktailPartyDataset as CocktailPartyDataset_GNN,
)

from datasets.gnn.matchnmingle.dataset import (
    MatchnMingleDataset as MatchNMingleDataset_GNN,
)
from datasets.dante.cocktail_party.dataset import (
    CocktailPartyDataset as CocktailPartyDataset_DANTE,
)
from datasets.dante.transforms.center_coordinate_frame import CenterCoordinateFrame
from datasets.dante.transforms.rectangular_angle import RectangularAngle
from datasets.dante.collate_fn import collate_fn
from datasets.dante.subset import Subset

from overwrites.geometric.point_pair_features import PointPairFeatures as PPF
from overwrites.ignite.precision import Precision

from models.gnn.gnn import GNN
from models.dante.dante import DANTE

from metrics.output_transforms import (
    output_transform_gnn,
    output_transform_dante,
)
from metrics.group_f1 import GroupF1Metric

from tensorboard import log_hyperparams, log_scalar


PRINT_INDICES = False
PRINT_DATASETS = True
PRINT_MODEL = True

CHECK_ASSERTS = True


def setup(args):
    random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)
    torch.set_num_threads(1)  # disable multiprocessing

    use_cuda = torch.cuda.is_available() and not args.no_cuda
    device = torch.device("cuda" if use_cuda else "cpu")

    run_str = "{0:%Y%m%d_%H%M%S}".format(datetime.now())
    model_name = f"{run_str}-m_{args.model}-l_{args.lr}-r_{args.weight_decay}"
    out_dir = os.path.join(args.log_dir, model_name)

    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)  # create logs dir

    return out_dir, device, use_cuda, model_name


def get_data_loaders_gnn(args, device):
    if args.dataset.startswith("cocktail_party"):
        if args.dataset == "cocktail_party":
            transforms = [Distance(norm=False)]
        elif args.dataset == "cocktail_party_head":
            transforms = [PPF(norm="head_norm")]
        elif args.dataset == "cocktail_party_body":
            transforms = [PPF(norm="body_norm")]
        else:
            transforms = [PPF(norm="head_norm"), PPF(norm="body_norm")]

        dataset = CocktailPartyDataset_GNN(
            root="./datasets/gnn/cocktail_party",
            transform=Compose([Center(), RadiusGraph(1e9)] + transforms),
            device=device,
        )

        train_indices, val_indices, test_indices = get_indices(
            len(dataset), args.num_folds, args.test_fold, args.percent_val
        )

    elif args.dataset.startswith("matchnmingle"):
        transforms = [Distance(norm=False)]
        root_dir = "./datasets/gnn/matchnmingle"
        if args.dataset == "matchnmingle_accel_label":
            root_dir = "./datasets/gnn/matchnmingle/accel_label"
        elif args.dataset == "matchnmingle_accel":
            root_dir = "./datasets/gnn/matchnmingle/accel"
        elif args.dataset == "matchnmingle_camera_0":
            root_dir = "./datasets/gnn/matchnmingle/camera_0"
        elif args.dataset == "matchnmingle_camera_1":
            root_dir = "./datasets/gnn/matchnmingle/camera_1"
        elif args.dataset == "matchnmingle_camera_2":
            root_dir = "./datasets/gnn/matchnmingle/camera_2"
        elif args.dataset == "matchnmingle_4_layers":
            root_dir = "./datasets/gnn/matchnmingle/layers_4"
        elif args.dataset == "matchnmingle_4_layers_camera_0":
            root_dir = "./datasets/gnn/matchnmingle/layers_4_camera_0"
        elif args.dataset == "matchnmingle_4_layers_camera_1":
            root_dir = "./datasets/gnn/matchnmingle/layers_4_camera_1"
        elif args.dataset == "matchnmingle_4_layers_camera_2":
            root_dir = "./datasets/gnn/matchnmingle/layers_4_camera_2"

        dataset = MatchNMingleDataset_GNN(
            root=root_dir,
            transform=Compose([Center(), RadiusGraph(1e9)] + transforms),
            device=device,
        )

        if args.sandwich_splitting:
            train_indices, val_indices, test_indices = sandwich_splitting(
                [ex.frame_id for ex in dataset],
                prop=1.0 / 5,
            )
        else:
            train_indices, val_indices, test_indices = split_train_test_by_camera_day(
                [ex.frame_id for ex in dataset], dataset=args.dataset
            )
    else:
        raise NotImplementedError("Dataset Not Available")

    if PRINT_INDICES:
        print(f"Train - {len(train_indices)} - {train_indices}")
        print(f"Validation- {len(val_indices)} - {val_indices}")
        print(f"Test - {len(test_indices)} - {test_indices}")

    num_pos, num_neg = 0, 0
    for i in train_indices:
        y = dataset[i].y.flatten()
        A_list = y_to_A_list(y)
        num_neg += (A_list[A_list == 0]).shape[0]
        num_pos += (A_list[A_list == 1]).shape[0]

    train_weights = []
    for i in train_indices:
        y = dataset[i].y.flatten()
        A_list = y_to_A_list(y)
        n_neg = (A_list[A_list == 0]).shape[0]
        n_pos = (A_list[A_list == 1]).shape[0]
        train_weights.append(n_neg / num_neg + n_pos / num_pos)

    sampler = WeightedRandomSampler(
        train_weights, num_samples=len(train_indices), replacement=True
    )

    train_dataset = dataset[train_indices]
    val_dataset = dataset[val_indices]
    test_dataset = dataset[test_indices]

    if PRINT_DATASETS:
        print("Train:", train_dataset)
        print("Validation:", val_dataset)
        print("Test:", test_dataset)

    if not args.random_sampler:
        train_loader = GeometricDataLoader(
            train_dataset, batch_size=args.batch_size, sampler=sampler
        )
    else:
        train_loader = GeometricDataLoader(
            train_dataset, batch_size=args.batch_size, shuffle=True
        )

    # to allow greater parallelization
    train_metrics_loader = GeometricDataLoader(
        train_dataset, batch_size=len(train_indices), shuffle=False
    )

    val_loader = GeometricDataLoader(
        val_dataset, batch_size=len(val_indices), shuffle=False
    )
    test_loader = GeometricDataLoader(
        test_dataset, batch_size=len(test_indices), shuffle=False
    )

    return (
        train_dataset,
        val_dataset,
        test_dataset,
        train_loader,
        train_metrics_loader,
        val_loader,
        test_loader,
    )


def get_data_loaders_dante(args, device, use_cuda):
    if args.dataset.startswith("cocktail_party"):
        name = "cocktail_party"
        if args.dataset == "cocktail_party":
            x, y, angles = 0, 1, []
        elif args.dataset == "cocktail_party_head":
            x, y, angles = 0, 1, [2]
        elif args.dataset == "cocktail_party_body":
            x, y, angles = 0, 1, [3]
        else:
            x, y, angles = 0, 1, [2, 3]

    elif args.dataset.startswith("matchnmingle"):
        x, y, angles = 0, 1, []
        name = "matchnmingle"
        if args.dataset == "matchnmingle_accel_label":
            name = "matchnmingle/accel_label"
        elif args.dataset == "matchnmingle_accel":
            name = "matchnmingle/accel"
        elif args.dataset == "matchnmingle_camera_0":
            name = "matchnmingle/camera_0"
        elif args.dataset == "matchnmingle_camera_1":
            name = "matchnmingle/camera_1"
        elif args.dataset == "matchnmingle_camera_2":
            name = "matchnmingle/camera_2"
        elif args.dataset == "matchnmingle_4_layers":
            name = "matchnmingle/layers_4"
        elif args.dataset == "matchnmingle_4_layers_camera_0":
            name = "matchnmingle/layers_4_camera_0"
        elif args.dataset == "matchnmingle_4_layers_camera_1":
            name = "matchnmingle/layers_4_camera_1"
        elif args.dataset == "matchnmingle_4_layers_camera_2":
            name = "matchnmingle/layers_4_camera_2"
    else:
        raise NotImplementedError("Dataset Not Available")

    # angles are the indices of the angle features in the dataset file
    # transform angles are the indices of the angle features in the pre_transform output
    transform_angles = list(range(2, 2 + len(angles)))
    transform = Compose(
        [
            CenterCoordinateFrame(
                x_index=x, y_index=y, angle_index=transform_angles, flip_y_prob=0.0
            ),
            RectangularAngle(angle_index=transform_angles),
        ]
    )

    # angle=[]->None, angle=[2]->head, angle=[3]->body, angle=[2,3]->head+body
    dataset = CocktailPartyDataset_DANTE(
        angles=angles,
        transform=transform,
        device=device,
        name=name,
    )

    if name.startswith("matchnmingle"):
        if args.sandwich_splitting:
            train_indices, val_indices, test_indices = sandwich_splitting(
                dataset.stamps,
                prop=1.0 / 5,
            )
        else:
            train_indices, val_indices, test_indices = split_train_test_by_camera_day(
                dataset.stamps, dataset=args.dataset
            )

    else:
        train_indices, val_indices, test_indices = get_indices(
            len(dataset), args.num_folds, args.test_fold, args.percent_val
        )

    if PRINT_INDICES:
        print(f"Train - {len(train_indices)} - {train_indices}")
        print(f"Validation- {len(val_indices)} - {val_indices}")
        print(f"Test - {len(test_indices)} - {test_indices}")

    train_dataset = Subset(dataset, train_indices)
    val_dataset = Subset(dataset, val_indices)
    test_dataset = Subset(dataset, test_indices)

    if PRINT_DATASETS:
        print("Train:", train_dataset)
        print("Validation:", val_dataset)
        print("Test:", test_dataset)

    data_loader_args = {
        "dataset": train_dataset,
        "batch_size": args.batch_size,
        "shuffle": args.random_sampler,
        "pin_memory": not use_cuda,
        "collate_fn": collate_fn,
    }

    if not args.random_sampler:
        train_weights = dataset.sample_weights(train_indices)
        sampler = WeightedRandomSampler(
            train_weights, num_samples=len(train_weights), replacement=True
        )
        data_loader_args["sampler"] = sampler

    train_loader = DataLoader(**data_loader_args)

    # remove sampler if present
    data_loader_args.pop("sampler", None)
    data_loader_args["batch_size"] = len(train_indices)
    train_metrics_loader = DataLoader(**data_loader_args)

    data_loader_args["batch_size"] = len(val_indices)
    data_loader_args["dataset"] = val_dataset
    val_loader = DataLoader(**data_loader_args)

    data_loader_args["batch_size"] = len(test_indices)
    data_loader_args["dataset"] = test_dataset
    test_loader = DataLoader(**data_loader_args)

    max_people = dataset.get_max_people()

    return (
        train_dataset,
        val_dataset,
        test_dataset,
        train_loader,
        train_metrics_loader,
        val_loader,
        test_loader,
        max_people,
    )


def get_model(args, train_dataset, max_people, device, out_dir=None):
    if args.model == "gnn":
        example = train_dataset[0]
        params = {
            "n_features": example.x.shape[1],
            "n_edge_features": example.edge_attr.shape[1],
            "n_edge_hidden_1": args.edge_hidden_1,
            "n_edge_hidden_2": args.edge_hidden_2,
            "n_edge_targets": [16, 1],
            "n_node_hidden_1": args.node_hidden_1,
            "n_node_hidden_2": args.node_hidden_2,
            "n_node_targets": [16, 1],
            "dropout": args.dropout,
            "thresh": args.thresh,
            "max_iter": args.max_iter,
            "min_eps": args.min_eps,
            "device": device,
            "expand_features": args.expand_features,
            "expand_dims": args.expand_dims,
            "img_mlp_features": args.img_mlp_features,
            "img_mlp_dims": args.img_mlp_dims,
            "early_stopping": args.early_stopping,
        }

        model = GNN(
            n_features=params["n_features"],
            n_edge_features=params["n_edge_features"],
            n_edge_hiddens_1=params["n_edge_hidden_1"],
            n_edge_hiddens_2=params["n_edge_hidden_2"],
            # n_edge_hiddens=params["n_edge_hiddens"],
            n_edge_targets=params["n_edge_targets"],
            n_node_hiddens_1=params["n_node_hidden_1"],
            n_node_hiddens_2=params["n_node_hidden_2"],
            # n_node_hiddens=params["n_node_hiddens"],
            n_node_targets=params["n_node_targets"],
            dropout=params["dropout"],
            device=params["device"],
            edge_projection_features=params["expand_features"],
            edge_projection_dims=params["expand_dims"],
            img_mlp_features=params["img_mlp_features"],
            img_mlp_dims=params["img_mlp_dims"],
        ).to(device)

    else:
        example = train_dataset[0][0]
        num_features = example[0][0][0].shape[0]

        params = {
            "input_feat": num_features,
            "dyad_mlp_dims": args.dyad_mlp,
            "context_mlp_dims": args.context_mlp,
            "dense_dims": args.dense_mlp,
            "dropout": args.dropout,
            "max_people": max_people,
            "device": device,
            "thresh": args.thresh,
            "max_iter": args.max_iter,
            "min_eps": args.min_eps,
            "expand_features": args.expand_features,
            "expand_dims": args.expand_dims,
            "img_mlp_features": args.img_mlp_features,
            "img_mlp_dims": args.img_mlp_dims,
            "early_stopping": args.early_stopping,
        }

        model = DANTE(
            input_feat=params["input_feat"],
            dyad_mlp_dims=params["dyad_mlp_dims"],
            context_mlp_dims=params["context_mlp_dims"],
            dense_dims=params["dense_dims"],
            dropout=params["dropout"],
            max_people=params["max_people"],
            device=params["device"],
            expand_features=params["expand_features"],
            expand_dims=params["expand_dims"],
            img_mlp_features=params["img_mlp_features"],
            img_mlp_dims=params["img_mlp_dims"],
        ).to(device)

    # save params
    if out_dir:
        with open(os.path.join(out_dir, "params.txt"), "w") as fid:
            json.dump(args.__dict__, fid, indent=2)

    if PRINT_MODEL:
        print(model)
        print()
        print("Total Parameters:", sum(p.numel() for p in model.parameters()))
        print(
            "Trainable Parameters:",
            sum(p.numel() for p in model.parameters() if p.requires_grad),
        )
        print("________________________________")
        print()

    return model


def get_trainer(
    args, train_loader, train_metrics_loader, val_loader, model, out_dir, device
):
    optimizer = Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay)
    loss_func = nn.BCELoss()  # or nn.MSELoss()

    if args.lr_step > 0:
        step_scheduler = StepLR(optimizer, step_size=args.lr_step, gamma=args.lr_decay)
        lr_scheduler = LRScheduler(step_scheduler)
    else:
        lr_scheduler = None

    def prepare_batch_gnn(batch, *args, **kwargs):
        y = []
        for i in range(batch.ptr.shape[0] - 1):
            curr_ptr, next_ptr = batch.ptr[i], batch.ptr[i + 1]
            y.append(batch.y[curr_ptr:next_ptr].flatten())
        return batch, y

    def prepare_batch_dante(batch, *args, **kwargs):
        return batch[0], batch[1]

    def _criterion_gnn(y_pred, y):
        loss, n = 0, len(y_pred)
        for edges, y_gt in zip(y_pred, y):
            A_list_gt = y_to_A_list(y_gt)
            A_list_pred = A_to_A_list(edges)
            loss += loss_func(A_list_pred, A_list_gt)
        return loss / n

    def _criterion_dante(y_pred, y):
        mask = y >= 0
        y_pred = y_pred[mask].flatten()
        y = y[mask].flatten()
        return loss_func(y_pred, y)

    if args.model == "gnn":
        prepare_batch = prepare_batch_gnn
        criterion = _criterion_gnn
    else:
        prepare_batch = prepare_batch_dante
        criterion = _criterion_dante

    trainer = create_supervised_trainer(
        model=model,
        optimizer=optimizer,
        loss_fn=criterion,
        prepare_batch=prepare_batch,
        device=device,
    )

    trainer.state.model = model

    # show progress bar when training
    pbar = ProgressBar(persist=True, bar_format="")
    pbar.attach(trainer)

    # save model
    # https://pytorch.org/ignite/handlers.html#ignite.handlers.ModelCheckpoint
    trainer.add_event_handler(
        Events.EPOCH_COMPLETED,
        ModelCheckpoint(
            os.path.join(out_dir, "checkpoint"),
            "",
            n_saved=2,
            create_dir=True,
            require_empty=False,
        ),
        {"model": model},
    )

    if lr_scheduler:
        trainer.add_event_handler(Events.EPOCH_COMPLETED, lr_scheduler)
        trainer.add_event_handler(
            Events.EPOCH_COMPLETED(every=args.stats_every), lambda: print(optimizer)
        )

    def f1_metric(threshold, out):
        return GroupF1Metric(
            model=args.model,
            group_threshold=threshold,
            out=out,
            threshold=args.thresh,
            max_iter=args.max_iter,
            min_eps=args.min_eps,
            device=device,
            stop_gc=args.early_stopping,
            cores=args.metric_cores,
        )

    metrics = {
        "loss": Loss(criterion),
        "group_f1t1": f1_metric(1.0, "f1"),
    }

    def transform_gnn(output):
        return output_transform_gnn(output, 1 / 2)

    def transform_dante(output):
        return output_transform_dante(output, 1 / 2)

    if args.model == "gnn":
        transform = transform_gnn
    else:
        transform = transform_dante

    precision = Precision(output_transform=transform, average=False)
    recall = Recall(output_transform=transform, average=False)
    F1 = (2 * precision * recall / (precision + recall + 1e-15)).mean()
    metrics["accuracy"] = Accuracy(output_transform=transform)
    metrics["f1"] = F1
    metrics["precision"] = precision
    metrics["recall"] = recall

    train_eval = create_supervised_evaluator(
        model, metrics=metrics, prepare_batch=prepare_batch, device=device
    )
    val_eval = create_supervised_evaluator(
        model, metrics=metrics, prepare_batch=prepare_batch, device=device
    )
    test_eval = create_supervised_evaluator(
        model, metrics=metrics, prepare_batch=prepare_batch, device=device
    )

    val_eval.add_event_handler(
        Events.COMPLETED,
        EarlyStopping(
            patience=args.patience,
            score_function=lambda x: -x.state.metrics["loss"],
            trainer=trainer,
        ),
    )

    val_eval.add_event_handler(
        Events.EPOCH_COMPLETED,
        ModelCheckpoint(
            os.path.join(out_dir, "best_model"),
            "group_f1t1",
            n_saved=1,
            create_dir=True,
            require_empty=False,
            score_function=lambda x: x.state.metrics["group_f1t1"],
        ),
        {"gnn": model},
    )

    def get_metrics(m):
        metrics = {}
        for metric in ["loss", "accuracy", "f1", "group_f1t1", "group_f1t2/3"]:
            if metric in m:
                metrics[metric] = m[metric]
        return metrics

    # callback for computing metrics
    @trainer.on(Events.EPOCH_COMPLETED(every=args.train_stats_every))
    def compute_train_metrics(engine):
        model.eval()
        train_eval.run(train_metrics_loader)
        model.train()

        print("T", get_metrics(train_eval.state.metrics))

    # callback for computing metrics
    @trainer.on(Events.EPOCH_COMPLETED(every=args.stats_every))
    def compute_val_metrics(engine):
        model.eval()
        val_eval.run(val_loader)
        model.train()

        print("V", get_metrics(val_eval.state.metrics))

    return trainer, train_eval, val_eval, test_eval


def get_logger(args, out_dir, trainer, train_eval, val_eval, test_eval, model_name):
    tb_logger = TensorboardLogger(log_dir=out_dir)

    tb_logger.attach(
        trainer,
        log_handler=OutputHandler(
            tag="training",
            output_transform=lambda loss: {"batchloss": loss},
            metric_names="all",
        ),
        event_name=Events.ITERATION_COMPLETED(every=10),
    )

    metric_names = [
        "loss",
        "group_f1t1",
        "accuracy",
        "f1",
        "precision",
        "recall",
    ]

    # log training metrics
    tb_logger.attach(
        train_eval,
        log_handler=OutputHandler(
            tag="training",
            metric_names=metric_names,
            global_step_transform=global_step_from_engine(trainer),
        ),
        event_name=Events.EPOCH_COMPLETED,
    )

    # log validation metrics
    tb_logger.attach(
        val_eval,
        log_handler=OutputHandler(
            tag="validation",
            metric_names=metric_names,
            global_step_transform=global_step_from_engine(trainer),
        ),
        event_name=Events.EPOCH_COMPLETED,
    )

    tb_logger.attach(
        val_eval,
        log_handler=log_hyperparams("hyperparams", args, model_name),
        event_name=Events.EPOCH_COMPLETED,
    )

    # log testing metrics
    tb_logger.attach(
        test_eval,
        log_handler=OutputHandler(
            tag="testing",
            metric_names=metric_names,
            global_step_transform=global_step_from_engine(trainer),
        ),
        event_name=Events.EPOCH_COMPLETED,
    )

    return tb_logger


def main():
    args = get_arguments()

    if args.model not in ["gnn", "dante"]:
        raise NotImplementedError("Model Not Available")

    if args.dataset not in [
        "cocktail_party",
        "cocktail_party_head",
        "cocktail_party_body",
        "cocktail_party_all",
        "matchnmingle",
        "matchnmingle_accel",
        "matchnmingle_accel_label",
        "matchnmingle_camera_0",
        "matchnmingle_camera_1",
        "matchnmingle_camera_2",
        "matchnmingle_4_layers",
        "matchnmingle_4_layers_camera_0",
        "matchnmingle_4_layers_camera_1",
        "matchnmingle_4_layers_camera_2",
    ]:
        raise NotImplementedError("Dataset Not Available")

    # setup pytorch seed, GPU settings, etc
    out_dir, device, use_cuda, model_name = setup(args)

    max_people = None  # only needed for DANTE

    print("Using ", args.model, " Model")

    # loads datasets, splits into train/val/test
    if args.model == "gnn":
        (
            train_dataset,
            val_dataset,
            test_dataset,
            train_loader,
            train_metrics_loader,
            val_loader,
            test_loader,
        ) = get_data_loaders_gnn(args, device)
    else:
        (
            train_dataset,
            val_dataset,
            test_dataset,
            train_loader,
            train_metrics_loader,
            val_loader,
            test_loader,
            max_people,
        ) = get_data_loaders_dante(args, device, use_cuda)

    # initializes model
    print("Loading model...")
    model = get_model(args, train_dataset, max_people, device, out_dir)

    # create centralized trainer object
    print("Getting trainer...")
    trainer, train_eval, val_eval, test_eval = get_trainer(
        args, train_loader, train_metrics_loader, val_loader, model, out_dir, device
    )

    # set up tensorboard logging
    print("Starting logger...")
    tb_logger = get_logger(
        args, out_dir, trainer, train_eval, val_eval, test_eval, model_name
    )

    # kick everything off
    print("Running model...")
    trainer.run(train_loader, max_epochs=args.epochs)

    tb_logger.close()
    print("Finished Training!")

    return test_eval.state.metrics["group_f1t1"]


if __name__ == "__main__":
    main()
