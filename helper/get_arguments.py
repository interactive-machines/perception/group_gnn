import os

import argparse


def get_arguments():
    """
    Read command line arguments and determine GPU usage. Returns
    `args`: argparse output, `use_cuda`: Boolean to use GPU or not
    """

    parser = argparse.ArgumentParser(description="End-to-End Clustering")

    """
    Add arguments here
    """

    """
    Random Seed
    """

    parser.add_argument("--seed", type=int, default=0, help="random seed (default: 0)")

    """
    Data Loading
    """

    parser.add_argument(
        "--dataset",
        type=str,
        default="cocktail_party_all",
        help="dataset path (default: cocktail_party_all)",
    )

    parser.add_argument(
        "--num-folds",
        type=int,
        default=5,
        metavar="N",
        help="number of folds (default: 5)",
    )

    parser.add_argument(
        "--test-fold",
        type=int,
        default=0,
        metavar="N",
        help="test fold (default: 0)",
    )

    parser.add_argument(
        "--percent-val",
        type=float,
        default=0.25,
        metavar="F",
        help="percent of training data to use for validation (default: 0.25)",
    )

    parser.add_argument(
        "--random-sampler",
        action="store_true",
        default=False,
        help="Use random sampling during training (instead of weighted sampler) (default: False)",
    )

    parser.add_argument(
        "--sandwich-splitting",
        action="store_true",
        default=False,
        help="Use sandwich index splitting technique - only for matchnmingle - 25% test, 25% val 50% train, 100% train, 50% train, 25% val, 25% test (default False)",
    )

    """
    Model Parameters
    """

    parser.add_argument(
        "--model",
        type=str,
        default="gnn",
        help="model type (default: gnn)",
    )

    parser.add_argument(
        "--epochs",
        type=int,
        default=1000,
        metavar="N",
        help="number of epochs to train (default: 1000)",
    )

    parser.add_argument(
        "--patience",
        type=int,
        default=50,
        metavar="N",
        help="patience for early stopping (default: 50)",
    )

    parser.add_argument(
        "--batch-size",
        type=int,
        default=64,
        metavar="N",
        help="input batch size for training (default: 64)",
    )

    parser.add_argument(
        "--lr",
        type=float,
        default=1e-4,
        metavar="F",
        help="learning rate (default: 1e-4)",
    )

    parser.add_argument(
        "--lr-step",
        type=int,
        default=0,
        help="learning rate decay step size (default: 0 - turns off lr decay)",
    )

    parser.add_argument(
        "--lr-decay",
        type=float,
        default=1e-1,
        help="learning rate decay rate - multiplied by lr every --lr-step epochs (default: 1e-1)",
    )

    parser.add_argument(
        "--weight-decay",
        type=float,
        default=0.01,
        metavar="F",
        help="weight decay for enabling L2 regularization (default: 0.01)",
    )

    parser.add_argument(
        "--dropout",
        type=float,
        default=0.25,
        help="Dropout prob? (default: 0.25)",
    )

    parser.add_argument(
        "--expand-features",
        type=int,
        default=0,
        help="Number of features to expand (starting from first index)",
    )

    parser.add_argument(
        "--expand-dims",
        nargs="+",
        type=int,
        default=[32, 124],
        help="List of feature expansion mlp dimensions (default: [32, 124])",
    )

    parser.add_argument(
        "--img-mlp-features",
        type=int,
        default=0,
        help="Number of img features to pass through img mlp (counting backward from last index)",
    )

    parser.add_argument(
        "--img-mlp-dims",
        nargs="+",
        type=int,
        default=[128, 64],
        help="List of img mlp dimensions (default: [128, 64])",
    )

    parser.add_argument(
        "--eig-loss",
        action="store_true",
        default=False,
        help="Use eigenvector distance loss? (default: False)",
    )

    """
    GNN Specific Parameters
    """

    parser.add_argument(
        "--edge-hidden-1",
        nargs="+",
        type=int,
        default=[128, 32],
        help="List of img mlp dimensions (default: [128, 64])",
    )

    parser.add_argument(
        "--edge-hidden-2",
        nargs="+",
        type=int,
        default=[16, 16],
        help="List of img mlp dimensions (default: [128, 64])",
    )

    parser.add_argument(
        "--node-hidden-1",
        nargs="+",
        type=int,
        default=[128, 32],
        help="List of img mlp dimensions (default: [128, 64])",
    )

    parser.add_argument(
        "--node-hidden-2",
        nargs="+",
        type=int,
        default=[16, 16],
        help="List of img mlp dimensions (default: [128, 64])",
    )

    """
    DANTE Specific Parameters
    """

    parser.add_argument(
        "--dyad-mlp",
        nargs="+",
        type=int,
        default=[32, 64],
        help="list of dyad transform mlp dimensions (default: [32, 64])",
    )

    parser.add_argument(
        "--context-mlp",
        nargs="+",
        type=int,
        default=[32, 64],
        help="list of context transform mlp dimensions (default: [32, 64])",
    )

    parser.add_argument(
        "--dense-mlp",
        nargs="+",
        type=int,
        default=[64, 32],
        help="list of mlp dimensions for the last dense layers (default: [64, 32])",
    )

    """
    DS Parameters
    """

    parser.add_argument(
        "--max-iter",
        type=int,
        default=200,
        metavar="N",
        help="Maximum DS iterations (default: 100)",
    )

    parser.add_argument(
        "--min-iter",
        type=int,
        default=0,
        metavar="N",
        help="Starting max iters for DS iteration increase scheduling (decrease 0, turned off)",
    )

    parser.add_argument(
        "--thresh",
        type=float,
        default=1e-5,
        metavar="N",
        help="DS threshold (default: 1e-5)",
    )

    parser.add_argument(
        "--min-eps",
        type=float,
        default=1e-15,
        metavar="N",
        help="Minimum epsilon for continuing DS iterations (default: 1e-5)",
    )

    parser.add_argument(
        "--early-stopping",
        action="store_true",
        default=False,
        help="Apply early stopping",
    )

    """
    Miscellaneous
    """

    parser.add_argument(
        "--log-dir", type=str, default="logs", help="logs directory (default: logs)"
    )

    parser.add_argument(
        "--no-cuda", action="store_true", default=False, help="disables CUDA training"
    )

    parser.add_argument(
        "--stats-every",
        type=int,
        default=20,
        help="Display Val F1 scores every __ epochs (default: 20)",
    )

    parser.add_argument(
        "--train-stats-every",
        type=int,
        default=100,
        help="Display Val F1 scores every __ epochs (default: 100)",
    )

    parser.add_argument(
        "--metric-cores",
        type=int,
        default=1,
        help="CPU cores to use for group_f1 calculations",
    )

    args = parser.parse_args()
    return args
