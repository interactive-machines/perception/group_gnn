import torch
import torch_geometric


class PointPairFeatures(object):
    def __init__(self, cat=True, pos="pos", norm="norm", edge_index="edge_index"):
        self.cat = cat
        self.pos = pos
        self.norm = norm
        self.edge_index = edge_index

    def __call__(self, data):
        ppf_func = torch_geometric.nn.conv.ppf_conv.point_pair_features

        pos = getattr(data, self.pos)
        norm = getattr(data, self.norm)
        edge_index = getattr(data, self.edge_index)

        assert pos is not None and norm is not None and edge_index is not None
        assert pos.size(-1) == 3 and pos.size() == norm.size()

        row, col = edge_index
        pseudo = data.edge_attr

        ppf = ppf_func(pos[row], pos[col], norm[row], norm[col])

        if pseudo is not None and self.cat:
            pseudo = pseudo.view(-1, 1) if pseudo.dim() == 1 else pseudo
            data.edge_attr = torch.cat([pseudo, ppf.type_as(pseudo)], dim=-1)
        else:
            data.edge_attr = ppf

        return data

    def __repr__(self):
        return "{}()".format(self.__class__.__name__)
