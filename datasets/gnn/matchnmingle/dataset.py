import os

import torch
from torch_geometric.data import InMemoryDataset, Data

from datasets.gnn.matchnmingle.read_dataset import read_dataset


class MatchnMingleDataset(InMemoryDataset):
    def __init__(self, root, transform=None, pre_transform=None, device="cpu"):
        self.root = root
        self.device = device
        super(MatchnMingleDataset, self).__init__(root, transform, pre_transform)
        self.data, self.slices = torch.load(self.processed_paths[0])
        self.data.to(device)

    @property
    def raw_file_names(self):
        data_dir = "data/matchnmingle"
        if "accel_label" in self.root:
            data_dir += "/accel_label"
        elif "accel" in self.root:
            data_dir += "/accel"
        if "layers_4" in self.root:
            data_dir += "/layers_4"
        if "camera" in self.root:
            if "layers_4" not in data_dir:
                data_dir += "/"
            else:
                data_dir += "_"
            cam = self.root[-1]
            data_dir += f"camera_{cam}"
        print("data_dir:", data_dir)

        return [data_dir]

    @property
    def processed_file_names(self):
        return ["data.pt"]

    @property
    def num_classes(self):
        if "4_layers" in self.root:
            return 4050
        elif "accel_label" in self.root:
            return 556
        elif "accel" in self.root:
            return 547
        else:
            return 516

    def parse_data_row(self, row):
        (frame_id, pos, actions, accelerometer, bounding_box, img_features, y) = row
        if "accel_label" in self.root:
            node_features = torch.cat((actions, accelerometer, img_features), dim=1)
        elif "accel" in self.root:
            node_features = torch.cat((accelerometer, img_features), dim=1)
        else:
            node_features = img_features
        data = Data(
            frame_id=frame_id,
            pos=pos,
            actions=actions,
            accelerometer=accelerometer,
            bounding_box=bounding_box,
            x=node_features,
            y=y,
        )
        return data

    def process(self):
        # Read data into huge `Data` list.
        data_list = []
        print(f"reading files from {self.raw_file_names[0]}")
        raw_data = read_dataset(self.raw_file_names[0])
        for row in raw_data:
            data = self.parse_data_row(row)
            data_list.append(data)

        if self.pre_filter is not None:
            data_list = [data for data in data_list if self.pre_filter(data)]

        if self.pre_transform is not None:
            data_list = [self.pre_transform(data) for data in data_list]

        data, slices = self.collate(data_list)
        torch.save((data, slices), self.processed_paths[0])
