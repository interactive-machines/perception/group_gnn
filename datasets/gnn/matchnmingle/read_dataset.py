import os

import torch

from datasets.gnn.utils import process_features, process_groups


def read_dataset(data_dir):
    if "layers_4" in data_dir:
        num_img_features = 4096
        other_features = 5
    elif "accel_label" in data_dir:
        num_img_features = 512
        other_features = 45
    elif "accel" in data_dir:
        num_img_features = 512
        other_features = 36
    else:
        num_img_features = 512
        other_features = 5
    print(f"expecting {num_img_features} image features")

    groups_file = os.path.join(data_dir, "groups.txt")
    features_file = os.path.join(data_dir, "features.txt")
    all_groups, all_features = {}, {}

    with open(groups_file, "r") as f:
        for line in f:
            if len(line) == 0:
                continue
            stamp = line.split(" ")[0]
            all_groups[stamp] = process_groups(line)

    with open(features_file, "r") as f:
        for line in f:
            if len(line) == 0:
                continue
            stamp = line.split(" ")[0]
            if stamp in all_groups and stamp not in all_features:
                processed_features = process_features(
                    line,
                    all_groups[stamp],
                    expected_num_features=num_img_features + other_features,
                )
                if processed_features:
                    all_features[stamp] = processed_features
                else:
                    all_groups.pop(stamp)

    if len(all_groups) != len(all_features):
        raise ValueError("Mismatched lengths")

    stamps = all_groups.keys()
    data = []
    for stamp in stamps:
        groups = all_groups[stamp]
        if stamp not in all_features:
            continue
        features = list(all_features[stamp].items())
        # {action} {has_accelerometer} {accelerometer_1}... {accelerometer_10} {height} {width}
        people = [f[0] for f in features]
        pos = torch.zeros(size=(len(people), 3))
        actions = torch.zeros(size=(len(people), 9))
        accelerometer = torch.zeros(size=(len(people), 31))
        bounding_box = torch.zeros(size=(len(people), 2))
        img_features = torch.zeros(size=(len(people), num_img_features))
        y = torch.zeros(size=(len(people), 1))
        for i, person in enumerate(people):
            pos[i] = torch.tensor(features[i][1][:2] + [0]) / 100
            if "accel_label" in data_dir:
                actions[i] = torch.tensor(features[i][1][2:11])
                accelerometer[i] = torch.tensor(features[i][1][11:42])
                bounding_box[i] = torch.tensor(features[i][1][42:44])
            elif "accel" in data_dir:
                accelerometer[i] = torch.tensor(features[i][1][2:33])
                bounding_box[i] = torch.tensor(features[i][1][33:35])
            try:
                img_features[i] = torch.tensor(features[i][1][-512:])
            except Exception as e:
                print(
                    f"EXCEPTION {e} - stamp: {stamp} - i: {i} - len(features[i]):",
                    len(features[i][1]),
                )
                continue
            for j, group in enumerate(groups):
                if person in group:
                    y[i] = torch.tensor(j)

        data.append([stamp, pos, actions, accelerometer, bounding_box, img_features, y])
    print(f"Found {data[0][3].shape} image features")

    print("unique views:", set([k[0][:3] for k in data]))
    # return combined lists
    return data
