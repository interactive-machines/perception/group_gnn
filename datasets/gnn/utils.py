import re
import random

"""
HELPER FUNCTIONS
"""


def process_groups(groups):
    groups_processed = []
    groups_arr = re.split(" < | > < ", groups)[1:]  # remove timestamp
    groups_processed = [re.split(" ", group.strip()) for group in groups_arr]
    groups_processed[-1] = groups_processed[-1][:-1]  # remove '\n'
    return groups_processed


def process_features(features, groups, expected_num_features=517):
    features_processed = {}

    num_people = sum([len(g) for g in groups])
    features = features.split(" ")[1:]  # removes timestamp
    features = [f for f in features if f not in ["", "\n"]]
    num_features = int(len(features) / num_people)

    if num_features != expected_num_features:
        print(
            f"num_people: {num_people} - num_features: {num_features} - len(features): {len(features)}"
        )
        return None

    for i in range(num_people):
        # people dict maps original IDs to standardized IDs
        features_processed[features[i * num_features]] = [
            float(x) for x in features[i * num_features + 1 : (i + 1) * num_features]
        ]

    return features_processed


def get_indices(num_frames, num_folds, test_fold, percent_val):
    if num_frames % num_folds != 0:
        raise ValueError("num_folds must evenly divide num_frames")

    if test_fold < 0 or test_fold >= num_folds:
        raise ValueError("enter valid test_fold")

    indices_per_fold = round(num_frames / num_folds)

    # compute test frames
    test_start = test_fold * indices_per_fold
    test_end = (test_fold + 1) * indices_per_fold
    test_indices = list(range(test_start, test_end))

    # compute validation frames
    num_val_frames = round((num_folds - 1) * indices_per_fold * percent_val)
    if test_fold == 0:
        val_indices = list(range(test_end, test_end + num_val_frames))
    elif test_fold == num_folds - 1:
        val_indices = list(range(test_start - num_val_frames, test_start))
    else:
        val_indices = list(range(test_start - round(num_val_frames / 2), test_start))
        val_indices.extend(list(range(test_end, test_end + round(num_val_frames / 2))))

    # compute train frames
    train_indices = [
        x for x in range(num_frames) if x not in test_indices and x not in val_indices
    ]

    return train_indices, val_indices, test_indices


def split_train_test_by_camera_day(frames, dataset):
    if "camera" in dataset:
        cam = dataset[-1]
        train_views = [f"0-{cam}"]
        val_views = [f"2-{cam}"]
        test_views = [f"1-{cam}"]
    else:
        train_views = ["0-1", "1-0", "1-2", "2-1"]
        val_views = ["0-2", "2-0"]
        test_views = ["0-0", "1-1", "2-2"]

    frame_starts = [frame[:3] for frame in frames]
    print("unique frame starts:", set(frame_starts))

    train_indices = [
        i
        for (i, frame_id) in enumerate(frames)
        if sum([frame_id.startswith(view) for view in train_views]) > 0
    ]
    val_indices = [
        i
        for (i, frame_id) in enumerate(frames)
        if sum([frame_id.startswith(view) for view in val_views]) > 0
    ]
    test_indices = [
        i
        for (i, frame_id) in enumerate(frames)
        if sum([frame_id.startswith(view) for view in test_views]) > 0
    ]

    print("train indices sample:", train_indices[:10])
    print("val_indices sample:", val_indices[:10])
    print("test_indices sample:", test_indices[:10])

    random.shuffle(train_indices)
    random.shuffle(val_indices)
    random.shuffle(test_indices)

    assert len(train_indices) + len(val_indices) + len(test_indices) == len(frames)

    return train_indices, val_indices, test_indices


def sandwich_splitting(frames, prop=1.0 / 4):
    """
    Splits each view (unique day/camera combination) into train/test/val.
    Takes prop/2 percentage of frames from beginning and end of view for test.
    Takes next prop/2 percentage of frames from beginning and end of view for val
    Takes center 2*prop percentage of frames for train.
    frames (list): list of frames ids of form <day>-<camera>-<global_frame_id>
    prop (float): Percentage of test/val frames. Train is 1-2*prop proportion of frames. Must be less than 1/4.
    """
    assert (
        prop <= 1.0 / 4
    )  # , f'proportion of test/val frames must be less than 1./4 - prop={prop}')

    frame_starts = [frame[:3] for frame in frames]
    print("unique frame starts:", set(frame_starts))

    train_indices = []
    val_indices = []
    test_indices = []

    frame_dict = {
        day_cam: [(i, id) for i, id in enumerate(frame_starts) if id[:3] == day_cam]
        for day_cam in frame_starts
    }

    for day_cam in set(frame_starts):
        day_cam_frames = frame_dict[day_cam]
        day_cam_frames.sort()
        num_prop_indices = int(len(day_cam_frames) * prop / 2)
        print(
            f"day_cam: {day_cam} - len(day_cam_frames): {len(day_cam_frames)} - num_prop_indices: {num_prop_indices}"
        )

        test_indices.extend([i for (i, frame_id) in day_cam_frames[:num_prop_indices]])
        val_indices.extend(
            [
                i
                for (i, frame_id) in day_cam_frames[
                    num_prop_indices : 2 * num_prop_indices
                ]
            ]
        )
        train_indices.extend(
            [
                i
                for (i, frame_id) in day_cam_frames[
                    2 * num_prop_indices : -2 * num_prop_indices
                ]
            ]
        )
        val_indices.extend(
            [
                i
                for (i, frame_id) in day_cam_frames[
                    -2 * num_prop_indices : -num_prop_indices
                ]
            ]
        )
        test_indices.extend([i for (i, frame_id) in day_cam_frames[-num_prop_indices:]])

    print("train_indices:", len(train_indices))
    print("val_indices:", len(val_indices))
    print("test_indices:", len(test_indices))

    random.shuffle(train_indices)
    random.shuffle(val_indices)
    random.shuffle(test_indices)

    assert len(train_indices) + len(val_indices) + len(test_indices) == len(frames)
    assert len(test_indices) == len(val_indices)

    return train_indices, val_indices, test_indices
