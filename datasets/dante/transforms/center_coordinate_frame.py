import random

import torch

from datasets.dante.utils import move_angle_to_PI_range


class CenterCoordinateFrame(object):
    """
    Center the data relative to a coordinate frame w.r.t. the dyad of interest
    """

    def __init__(self, x_index=0, y_index=1, angle_index=[2], flip_y_prob=0):
        """
        Constructor
        :param x_index: (int) position of the x coordinate feature in the second dim of the input tensors
        :param x_index: (int) position of the y coordinate feature in the second dim of the input tensors
        :param angle_index: (list) position of the orientation features in the second dim of the input tensors
        :param flip_y_prob: prob for flipping vertically the data after centering w.r.t. the dyad (default: 0 for no flip)
        """
        self.x_index = x_index
        self.y_index = y_index
        self.angle_index = angle_index
        self.flip_y_prob = flip_y_prob

    def _transform_tensor(self, tensor, center, gamma):
        """
        Helper function to transform the inputs in a sample
        :param tensor: 2D input tensor
        :param center: center of the new cooordinate frame (it's between the dyad)
        :param gamma: orientation (in radians) of the x axis of the new coordinate frame
        :return: transformed tensor
        """
        N = tensor.shape[0]

        # translate position (center frame)
        tensor[:, self.x_index] -= center[0].repeat(N)
        tensor[:, self.y_index] -= center[1].repeat(N)

        # rotate position (orient frame)
        c = torch.cos(-gamma)
        s = torch.sin(-gamma)
        new_x = c * tensor[:, self.x_index] - s * tensor[:, self.y_index]
        new_y = s * tensor[:, self.x_index] + c * tensor[:, self.y_index]
        tensor[:, self.x_index] = new_x
        tensor[:, self.y_index] = new_y

        # rotate orientations
        for a in self.angle_index:
            tensor[:, a] -= gamma.repeat(N)
        return tensor

    def _flip_y(self, dyad, context):
        """
        Helper function to transform a given dyad and context by flipping the context
        vertically relative to the dyad's coordinate system
        :param dyad: dyad tensor
        :param context: context tensor
        :return: transformed dyad and context tensor
        """
        if random.random() < self.flip_y_prob:
            # update dyad angles
            for i in range(dyad.shape[0]):
                for a in self.angle_index:
                    dyad[i, a] = -move_angle_to_PI_range(dyad[i, a])

            # update context
            for i in range(context.shape[0]):
                context[i, self.y_index] = -context[i, self.y_index]
                for a in self.angle_index:
                    context[i, a] = -move_angle_to_PI_range(context[i, a])

        return dyad, context

    def __call__(self, sample):
        """
        Transform a given sample (dyad_input, context_input)
        :param sample: AffinityDataset sample
        """
        dyad_input, context_input = sample

        center = torch.sum(dyad_input[:, [self.x_index, self.y_index]], 0) * 0.5
        dx = dyad_input[0, self.x_index] - dyad_input[1, self.x_index]
        dy = dyad_input[0, self.y_index] - dyad_input[1, self.y_index]
        gamma = torch.atan2(dy, dx)

        new_dyad = self._transform_tensor(dyad_input, center, gamma)
        new_context = self._transform_tensor(context_input, center, gamma)

        if self.flip_y_prob > 0:
            new_dyad, new_context = self._flip_y(new_dyad, new_context)

        return (new_dyad, new_context)

    def __repr__(self):
        return (
            self.__class__.__name__
            + "(x_index={}, y_index={}, angle_index={})".format(
                self.x_index, self.y_index, self.angle_index
            )
        )
