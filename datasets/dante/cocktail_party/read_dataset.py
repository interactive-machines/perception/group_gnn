import re


def read_dataset(angles, name="cocktail_party"):
    """
    Read data from directory and returns raw features and groups
    :param data_dir: directory of data (containing features.txt, groups.txt)
    """

    groups_file = f"data/{name}/groups.txt"
    features_file = f"data/{name}/features.txt"

    all_groups = {}  # dict of stamp mapping to group
    all_people_dict = {}  # temporary, not passed out
    all_features = {}  # dict of stamp mapping to features dict
    all_num_people = {}

    # read groups.txt file
    with open(groups_file, "r") as f:
        for line in f:
            # ignore empty lines
            if len(line) == 0:
                continue

            stamp = line.split(" ")[0]
            groups, people_dict = process_groups(line)
            all_groups[stamp] = groups
            all_people_dict[stamp] = people_dict

    with open(features_file, "r") as f:
        for line in f:
            # ignore empty lines
            if len(line) == 0:
                continue

            stamp = line.split(" ")[0]
            if stamp in all_groups and stamp not in all_features:
                features, num_people = process_features(line, all_people_dict[stamp])

                new_features = {}
                for key in features.keys():
                    # possibly hides head/body angles
                    if name == "matchnmingle/accel_label":
                        new_features[key] = features[key][:42] + features[key][44:]
                    elif name == "matchnmingle/accel":
                        new_features[key] = features[key][:33] + features[key][35:]
                    elif name.startswith("matchnmingle"):
                        new_features[key] = features[key][:2] + features[key][4:]
                    else:
                        new_features[key] = features[key][:2]
                        for angle in angles:
                            new_features[key] += [features[key][angle]]
                all_features[stamp] = new_features
                all_num_people[stamp] = num_people

    if len(all_groups) != len(all_features):
        raise ValueError("Mismatched lengths")

    # return combined lists
    return all_groups, all_features, all_num_people


def process_groups(groups):
    """
    processes a single line of data from the groups.txt file. Returns `groups_processed`:
    a list of groups (each a list of IDs), and `people_dict`: mapping from original IDs to new IDs
    :param groups: raw string from groups.txt file
    """
    groups_processed = []
    groups_arr = re.split(" < | > < ", groups)[1:]  # remove timestamp
    groups_processed = [re.split(" ", group.strip()) for group in groups_arr]
    groups_processed[-1] = groups_processed[-1][:-1]  # remove '\n'

    # standardize IDs to be ID_001, ID_002, ...
    people = [person for group in groups_processed for person in group]
    people_dict = {p: i for i, p in enumerate(people)}
    groups_processed = [[people_dict[p] for p in g] for g in groups_processed]
    return groups_processed, people_dict


def process_features(features, people_dict):
    """
    processes a single line of data from the features.txt file. Returns
    `features_processed`: a dictionary mapping IDs to their corresponding features
    :param features: raw string from features.txt file
    :param people_dict: dictionary mapping original IDs to standardized IDs
    """
    features_processed = {}

    num_people = len(people_dict.keys())
    features = features.split(" ")[1:]  # removes timestamp
    features = [f for f in features if f not in ["", "\n"]]
    num_features = int(len(features) / num_people)

    for i in range(num_people):
        # people dict maps original IDs to standardized IDs
        standard_person_id = people_dict[features[i * num_features]]
        features_processed[standard_person_id] = [
            float(x) for x in features[i * num_features + 1 : (i + 1) * num_features]
        ]

    return features_processed, num_people
