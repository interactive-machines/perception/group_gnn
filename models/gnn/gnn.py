import torch
import torch.nn as nn

from models.gnn.utils import build_layer, ListModule
from models.gnn.expansion_model import ExpansionModel


class GNN(nn.Module):
    def __init__(
        self,
        n_features,
        n_edge_features,
        n_edge_hiddens_1,
        n_edge_hiddens_2,
        # n_edge_hiddens,
        n_edge_targets,
        n_node_hiddens_1,
        n_node_hiddens_2,
        # n_node_hiddens,
        n_node_targets,
        dropout,
        device,
        edge_projection_features=None,
        edge_projection_dims=[],
        img_mlp_features=0,
        img_mlp_dims=[],
    ):
        super().__init__()

        self.edge_projection_features = edge_projection_features
        if self.edge_projection_features > 0:
            print("n_edge_features:", n_edge_features)
            print("self.edge_projection_features:", self.edge_projection_features)
            print("edge_projection_dims:", edge_projection_dims[-1])
            self.expansion_model = ExpansionModel(n_edge_features, edge_projection_dims)
            n_edge_features = (
                n_edge_features
                - self.edge_projection_features
                + edge_projection_dims[-1]
            )
            print("n_edge_features:", n_edge_features)
        else:
            self.expansion_model = None

        self.img_mlp_features = img_mlp_features
        if self.img_mlp_features > n_features:
            raise ValueError("Cannot have more img_mlp than input features")
        if self.img_mlp_features > 0 and len(img_mlp_dims) > 0:
            print("n_features:", n_features)
            print("self.img_mlp_features:", self.img_mlp_features)
            print("img_projection_dims:", img_mlp_dims[-1])
            self.img_mlp = ExpansionModel(self.img_mlp_features, img_mlp_dims)
            n_features = n_features - self.img_mlp_features + img_mlp_dims[-1]
        elif self.img_mlp_features == -1:
            n_features = n_features - 512
            self.img_mlp = None
        else:
            self.img_mlp = None

        # for [i-1] wrap around
        n_node_targets.append(n_features)
        n_edge_targets.append(n_edge_features)

        self.meta_layers = ListModule(self, "meta_")
        # for i in range(len(n_edge_hiddens)):
        #     self.meta_layers.append(
        #         build_layer(
        #             n_features=n_node_targets[i - 1],
        #             n_edge_features=n_edge_targets[i - 1],
        #             n_edge_hiddens=n_edge_hiddens[i],
        #             n_edge_targets=n_edge_targets[i],
        #             n_node_hiddens=n_node_hiddens[i],
        #             n_node_targets=n_node_targets[i],
        #             dropout=dropout,
        #         )
        #     )
        self.meta_layers.append(
            build_layer(
                n_features=n_node_targets[-1],
                n_edge_features=n_edge_targets[-1],
                n_edge_hiddens=n_edge_hiddens_1,
                n_edge_targets=n_edge_targets[0],
                n_node_hiddens=n_node_hiddens_1,
                n_node_targets=n_node_targets[0],
                dropout=dropout,
            )
        )
        self.meta_layers.append(
            build_layer(
                n_features=n_node_targets[0],
                n_edge_features=n_edge_targets[0],
                n_edge_hiddens=n_edge_hiddens_2,
                n_edge_targets=n_edge_targets[1],
                n_node_hiddens=n_node_hiddens_2,
                n_node_targets=n_node_targets[1],
                dropout=dropout,
            )
        )

        self.device = device

    def _forward(self, data):
        nodes, edges = data.x, data.edge_attr
        if self.expansion_model:
            edges = self.expansion_model(edges)

        if self.img_mlp:
            node_img = self.img_mlp(nodes[:, -self.img_mlp_features :])
            nodes = torch.cat((nodes[:, : -self.img_mlp_features], node_img), dim=1)
        elif self.img_mlp_features == -1:
            # remove image features
            nodes = nodes[:, :-512]

        for layer in self.meta_layers:
            nodes, edges, _ = layer.forward(nodes, data.edge_index, edges, None, None)
        edges = torch.sigmoid(edges)

        data_n = data.x.shape[0]
        A = torch.zeros((data_n, data_n), dtype=torch.float, device=self.device)
        A[data.edge_index[0], data.edge_index[1]] = edges.flatten()

        output = []
        for i in range(data.ptr.shape[0] - 1):
            curr_ptr, next_ptr = data.ptr[i], data.ptr[i + 1]
            A_i = A[curr_ptr:next_ptr, curr_ptr:next_ptr]
            n = A_i.shape[0]
            triu = torch.triu_indices(n, n, 1)
            A_upper = A_i[triu[0], triu[1]]
            A_lower = A_i.T[triu[0], triu[1]]
            # alternatives to mean function
            A_sym = (A_upper + A_lower) / 2
            # A_sym = torch.max(A_upper, A_lower)
            # A_sym = torch.min(A_upper, A_lower)
            A_new = torch.zeros_like(A_i)
            A_new[triu[0], triu[1]] = A_sym
            A_new = A_new + A_new.T
            output.append(A_new)
        return output

    def forward(self, data):
        return self._forward(data)
