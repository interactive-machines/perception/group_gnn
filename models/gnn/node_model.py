from collections import OrderedDict

import torch
import torch.nn as nn

from torch_geometric.nn import MessagePassing

from torch_scatter import scatter_mean


class NodeModel(nn.Module):
    def __init__(self, n_features, n_edge_features, n_hiddens, n_targets, dropout):
        super(NodeModel, self).__init__()
        h = n_hiddens  # shorter notation

        mlp_1_list = [("conv_1", nn.Linear(n_features + n_edge_features, h[0]))]
        mlp_1_list.append(("relu_1", nn.ReLU()))
        if dropout > 0:
            mlp_1_list.append(("dropout_1", nn.Dropout(dropout)))
        mlp_1_list.append(("bn_1", nn.BatchNorm1d(h[0])))
        for i in range(1, len(h)):
            mlp_1_list.append(("conv_" + str(i + 1), nn.Linear(h[i - 1], h[i])))
            mlp_1_list.append(("relu_" + str(i + 1), nn.ReLU()))
            if dropout > 0:
                mlp_1_list.append(("dropout_" + str(i + 1), nn.Dropout(dropout)))
            mlp_1_list.append(("bn_" + str(i + 1), nn.BatchNorm1d(h[i])))
        mlp_1_list.append(("conv_" + str(len(h) + 1), nn.Linear(h[-1], n_targets)))
        self.node_mlp_1 = nn.Sequential(OrderedDict(mlp_1_list))

        mlp_2_list = [("conv_1", nn.Linear(n_targets + n_features, h[0]))]
        mlp_2_list.append(("relu_1", nn.ReLU()))
        if dropout > 0:
            mlp_2_list.append(("dropout_1", nn.Dropout(dropout)))
        mlp_2_list.append(("bn_1", nn.BatchNorm1d(h[0])))
        for i in range(1, len(h)):
            mlp_2_list.append(("conv_" + str(i + 1), nn.Linear(h[i - 1], h[i])))
            mlp_2_list.append(("relu_" + str(i + 1), nn.ReLU()))
            if dropout > 0:
                mlp_2_list.append(("dropout_" + str(i + 1), nn.Dropout(dropout)))
            mlp_2_list.append(("bn_" + str(i + 1), nn.BatchNorm1d(h[i])))
        mlp_2_list.append(("conv_" + str(len(h) + 1), nn.Linear(h[-1], n_targets)))
        self.node_mlp_2 = nn.Sequential(OrderedDict(mlp_2_list))

    def forward(self, x, edge_index, edge_attr, u=None, batch=None):
        row, col = edge_index
        out = torch.cat([x[col], edge_attr], dim=1)
        out = self.node_mlp_1(out)
        out = scatter_mean(out, row, dim=0, dim_size=x.size(0))
        out = torch.cat([x, out], dim=1)
        out = self.node_mlp_2(out)
        return out


class AffinityNodeModel(MessagePassing):
    def __init__(
        self, n_features, n_edge_features, n_hiddens, n_targets, dropout, aggr="mean"
    ):
        super(AffinityNodeModel, self).__init__(aggr)
        h = n_hiddens  # shorter notation

        mlp_list = [("conv_1", nn.Linear(2 * n_features + n_edge_features, h[0]))]
        mlp_list.append(("relu_1", nn.ReLU()))
        if dropout > 0:
            mlp_list.append(("dropout_1", nn.Dropout(dropout)))
        mlp_list.append(("bn_1", nn.BatchNorm1d(h[0])))
        for i in range(1, len(h)):
            mlp_list.append(("conv_" + str(i + 1), nn.Linear(h[i - 1], h[i])))
            mlp_list.append(("relu_" + str(i + 1), nn.ReLU()))
            if dropout > 0:
                mlp_list.append(("dropout_" + str(i + 1), nn.Dropout(dropout)))
            mlp_list.append(("bn_" + str(i + 1), nn.BatchNorm1d(h[i])))
        mlp_list.append(("conv_" + str(len(h) + 1), nn.Linear(h[-1], n_targets)))
        self.node_mlp = nn.Sequential(OrderedDict(mlp_list))

    def forward(self, x, edge_index, edge_attr, edge_affinity):
        out = self.propagate(
            edge_index, x=x, edge_attr=edge_attr, edge_affinity=edge_affinity
        )
        out = self.node_mlp(out)
        return out

    def message(self, x_j, edge_attr, edge_affinity):
        return edge_affinity * torch.cat((x_j, edge_attr), dim=1)

    def update(self, aggr_out, x):
        return torch.cat((x, aggr_out), dim=1)
