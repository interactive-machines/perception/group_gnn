from collections import OrderedDict

import torch
import torch.nn as nn


class EdgeModel(nn.Module):
    def __init__(self, n_features, n_edge_features, n_hiddens, n_targets, dropout):
        super().__init__()
        h = n_hiddens  # shorter notation

        mlp_list = [("conv_1", nn.Linear(2 * n_features + n_edge_features, h[0]))]
        mlp_list.append(("relu_1", nn.ReLU()))
        if dropout > 0:
            mlp_list.append(("dropout_1", nn.Dropout(dropout)))
        mlp_list.append(("bn_1", nn.BatchNorm1d(h[0])))
        for i in range(1, len(h)):
            mlp_list.append(("conv_" + str(i + 1), nn.Linear(h[i - 1], h[i])))
            mlp_list.append(("relu_" + str(i + 1), nn.ReLU()))
            if dropout > 0:
                mlp_list.append(("dropout_" + str(i + 1), nn.Dropout(dropout)))
            mlp_list.append(("bn_" + str(i + 1), nn.BatchNorm1d(h[i])))
        mlp_list.append(("conv_" + str(len(h) + 1), nn.Linear(h[-1], n_targets)))
        self.edge_mlp = nn.Sequential(OrderedDict(mlp_list))

    def forward(self, src, dest, edge_attr, u=None, batch=None):
        out = torch.cat([src, dest, edge_attr], 1)
        out = self.edge_mlp(out)
        return out
