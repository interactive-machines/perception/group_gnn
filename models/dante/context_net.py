import torch
import torch.nn as nn
import torch.nn.functional as F

from models.dante.utils import check_dims, create_shared_mlp


class ContextNet(nn.Module):
    """DANTE's context transform"""

    def __init__(
        self,
        input_feat,
        mlp_dims=[32, 128],
        max_pool=True,
        add_batchnorm=False,
        add_layernorm=True,
        dropout=None,
    ):
        """
        Constructor
        :param input_feat: number of features per person in the model
        :param mlp_dims: number of features in the individual mlp
        :param max_pool: use max/avg pooling to combine features (default: max)
        :param add_batchnorm: apply batchnorm? (default: False)
        :param add_layernorm: apply layernorm? (default: True)
        :param dropout: dropout probability (default: None, no dropout)
        """
        super(ContextNet, self).__init__()

        self.input_feat = input_feat
        self.max_pool = max_pool
        self.mlp_dims = mlp_dims

        # create shared mlp using 2D convolutions
        self.mlp = create_shared_mlp(
            prefix="Context",
            input_feat=self.input_feat,
            mlp_dims=self.mlp_dims,
            batchnorm=add_batchnorm,
            layernorm=add_layernorm,
            dropout=dropout,
        )

    def forward(self, x):
        """
        Forward operation
        :param x: input tensor (B * N, P - 2, F) where B is batch size, where N is number of samples per frame, P is #people, and F is #feat per person
        :return: output tensor (B * N, K) where K is the number of features in the context vector
        """
        # sanity checks
        check_dims(x, [None, None, self.input_feat])
        num_pairs = x.shape[0]
        num_people = x.shape[1]

        # do not compute context when there are only 2 people to consider
        if num_people == 0:
            output_dim = self.mlp_dims[1]
            features = torch.zeros(num_pairs, output_dim, device=x.device)
            return features

        # project into higher dimensional space. We end up having a tensor with shape (B * N) x (P - 2) x K
        features = x
        for layer in self.mlp:
            if isinstance(layer, nn.BatchNorm1d):
                # reshape the tensor to normalize the features based on the batch
                tmp = features.view(-1, features.shape[-1])
                if tmp.shape[0] > 1:
                    tmp = layer(tmp)
                features = tmp.view(num_pairs, num_people, features.shape[-1])
            else:
                features = layer(features)

        # transpose dimensions. Now we have a tensor that is (B * N) x K x (P - 2)
        features = features.transpose(2, 1)

        # pool across the last dimensions to get a tensor of shape (B * N) x K x 1
        dims = features.size()
        tmp = features.view(-1, features.shape[-2], features.shape[-1])
        if self.max_pool:
            # note that we don't define the pooling operation in the init function of this
            # class because that would require to know the number of people before calling the forward function
            features = F.max_pool1d(
                tmp,  # input
                (dims[-1],),  # kernel size (number of people)
                1,  # stride
                0,  # padding
                1,  # dilation
                False,  # ceil mode
                False,  # return indices
            )
        else:
            # use avg pooling
            features = F.avg_pool1d(
                tmp,  # input
                (dims[-1],),  # kernel size (number of people)
                1,  # stride
                0,  # padding
                False,  # ceil mode
                False,  # count include pad
            )

        features = features.reshape(num_pairs, -1)
        return features
