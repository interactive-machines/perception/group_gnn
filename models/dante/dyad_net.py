import torch.nn as nn

from models.dante.utils import check_dims, create_shared_mlp


class DyadNet(nn.Module):
    """DANTE's dyad transform"""

    def __init__(
        self,
        input_feat,
        mlp_dims=[32, 128],
        add_batchnorm=False,
        add_layernorm=False,
        dropout=None,
    ):
        """
        Constructor
        :param input_feat: number of features per person in the model
        :param mlp_dims: number of features in the individual mlp
        :param add_batchnorm: apply batchnorm? (default: True)
        :param dropout: dropout probability (default: None, no dropout)
        """
        super(DyadNet, self).__init__()

        self.input_feat = input_feat
        self.mlp_dims = mlp_dims

        self.mlp = create_shared_mlp(
            prefix="Dyad",
            input_feat=self.input_feat,
            mlp_dims=self.mlp_dims,
            batchnorm=add_batchnorm,
            layernorm=add_layernorm,
            dropout=dropout,
        )

    def forward(self, x):
        """
        Forward operation
        :param x: input tensor (B * N, 2, F) where B is batch size, where N is number of samples per frame, 2 is #people, and F is #feat per person
        :return: output tensor (B * N, K) where K is the number of features in the dyad representation
        """

        # sanity checks
        check_dims(x, [None, 2, self.input_feat])
        num_pairs = x.shape[0]
        num_people = x.shape[1]  # 2

        features = x
        for layer in self.mlp:
            if isinstance(layer, nn.BatchNorm1d) or isinstance(layer, nn.LayerNorm):
                # reshape the tensor to normalize the features based on the batch
                tmp = features.view(-1, features.shape[-1])
                if tmp.shape[0] > 1:
                    tmp = layer(tmp)
                features = tmp.view(num_pairs, num_people, features.shape[-1])
            else:
                features = layer(features)

        # flatten
        features = features.reshape(num_pairs, -1)
        return features
