import torch.nn as nn

from models.dante.utils import create_shared_mlp


class ExpansionModel(nn.Module):
    def __init__(self, n_features, mlp_dims):
        """
        Model to handle dimension expansion
        n_features (int): number of input features
        mlp_dims (list): list of internal dimensions
        Example: n_features=4, mlp_dims=[8,16,64]
        input
        """
        super().__init__()
        self.mlp = create_shared_mlp(
            "expansion",
            n_features,
            mlp_dims,
            batchnorm=True,
            layernorm=False,
            dropout=0,
        )

    def forward(self, x):
        """
        Passes x through expansion mlp
        x: shape must be (num_pairs, num_people, num_features)
        output: shaped (num_pairs, num_people, output_dims), where output_dims is mlp_dims[-1] (from constructor)
        """
        num_pairs, num_people = x.shape[0], x.shape[1]
        # out = self.mlp(x)
        out = x
        for layer in self.mlp:
            if isinstance(layer, nn.BatchNorm1d):
                # reshape the tensor to normalize the features based on the batch
                tmp = out.view(-1, out.shape[-1])
                if tmp.shape[0] > 1:
                    tmp = layer(tmp)
                out = tmp.view(num_pairs, num_people, out.shape[-1])
            else:
                out = layer(out)
        return out
