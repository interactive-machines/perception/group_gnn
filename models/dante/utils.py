import warnings
from collections import OrderedDict

from torch.jit import TracerWarning
import torch.nn as nn


def check_dims(x, shape):
    """
    Helper method to check shape of an input tensor w.r.t. a given shape
    :param x: input tensor
    :param dims: expected shape (use None for no check)
    """
    # we first disable jit warnings so that the check of dimensions doesn't trigger a printed warning in the terminal
    # for more info, see: https://github.com/pytorch/pytorch/issues/19364
    with warnings.catch_warnings():
        warnings.filterwarnings(action="ignore", category=TracerWarning, module=r".*")

        if len(x.shape) != len(shape):
            raise RuntimeError(
                "Expected the input tensor to have {} dimensions, but got {} (shape={})".format(
                    len(shape), len(x.shape), x.shape
                )
            )

        for i in range(len(shape)):
            if shape[i] is not None and x.shape[i] != shape[i]:
                raise RuntimeError(
                    "Expected the dimension {} of the input tensor to be {} but got {}".format(
                        i, shape[i], x.shape[i]
                    )
                )


def create_shared_mlp(prefix, input_feat, mlp_dims, batchnorm, layernorm, dropout):
    """
    Creates a shared mlp computation graph. The implementation
    uses 2D convolutions to mimic multilayer perceptrons
    :param prefix: prefix for layer names
    :param input_feat: number of input features
    :param mlp_dims: list with dimensionality of the mlps
    :param batchnorm: boolean to add batch normalization
    :param dropout: float dropout percentage
    :return: ModuleList with CNN layers
    """
    layers = []
    for i in range(len(mlp_dims)):
        # create dense layer
        layers.append(
            ("{}_Dense_{}".format(prefix, i), nn.Linear(input_feat, mlp_dims[i]))
        )
        # save the output dims for the next conv layer
        input_feat = mlp_dims[i]
        # create activation
        layers.append(("{}_RELU_{}".format(prefix, i), nn.ReLU()))
        # apply dropout
        if dropout > 0:
            layers.append(("{}_Dropout_{}".format(prefix, i), nn.Dropout(dropout)))
        # apply batch norm
        if batchnorm:
            layers.append(("{}_BN_{}".format(prefix, i), nn.BatchNorm1d(input_feat)))
        # apply layer norm
        if layernorm:
            layers.append(("{}_BN_{}".format(prefix, i), nn.LayerNorm(input_feat)))

    return nn.Sequential(OrderedDict(layers))
